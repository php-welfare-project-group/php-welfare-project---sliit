<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->post('/api/worksin', function(Request $request, Response $response, array $args){
    $empId = $request->getParam('empId');
    $sectionId = $request->getParam('sectionId');
    $startDate = $request->getParam('startDate');
    $endDate = $request->getParam('endDate');

    $sql = "INSERT INTO worksin(empId,sectionId,startDate) VALUES(:empId,:sectionId,:startDate)";

    if($endDate != null){
        $sql = "INSERT INTO worksin(empId,sectionId,startDate,endDate) VALUES(:empId,:sectionId,:startDate,:endDate)";
    }

    try{
        $db = new db();
        $db = $db->connect();

        $stmt = $db->prepare($sql);

        $stmt->bindParam(':empId', $empId);
        $stmt->bindParam(':sectionId', $sectionId);
        $stmt->bindParam(':startDate', $startDate);

        if($endDate != null){
            $stmt->bindParam(':endDate', $endDate);
        }

        $stmt->execute();

        $db = null;

        return $response->withJSON(['message' => 'Successfully Assigned'],203,JSON_UNESCAPED_UNICODE);

    } catch(PDOException $e){
        return $response->withJSON(
        ['error' => 'Internal server error',
        'system_error' => $e->getMessage(),
        'response' => '500'],
        500,
        JSON_UNESCAPED_UNICODE);
    }
});
