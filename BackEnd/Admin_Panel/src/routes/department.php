<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/api/department', function (Request $request, Response $response, array $args) {

	 $sql = "SELECT * FROM department";
   try{
        $db = new db();
        $db = $db->connect();

        $stmt = $db->query($sql);
        $branches = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
				$rowCount = $stmt->rowCount();
		if($rowCount > 0){
			return $response->withJSON(['data' => $branches,'rowCount' => $rowCount],200,JSON_UNESCAPED_UNICODE);
		}
		else{
			return $response->withJSON(['message' => 'No content','status' => '204','rowCount' => $rowCount],204,JSON_UNESCAPED_UNICODE);
		}

     } catch(PDOException $e){
		return $response->withJSON(
			['error' => 'Internal server error',
			'system_error' => $e->getMessage(),
			'response' => '500'],
			500,
			JSON_UNESCAPED_UNICODE);
     }
});
