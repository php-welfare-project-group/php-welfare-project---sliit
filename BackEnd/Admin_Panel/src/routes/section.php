<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

// Get all sections

$app->get('/api/section', function (Request $request, Response $response, array $args) {
  $startPosition = $request->getHeaderLine('startPosition');
	$branchId = $request->getHeaderLine('branchId');
	$active = $request->getHeaderLine('active');

	$sql = "SELECT section.sectionId, section.sectionName, section.branchId, section.status, branch.name
					FROM section
					JOIN branch ON section.branchId = branch.branchId ";
	
	if($active != ""){
		$sql .= "WHERE section.status = 1";	 
	}
	else{
		$sql .= "WHERE section.status != -1";
	}				

	if($branchId != ""){
		$sql .= " AND section.branchId =".$branchId;
	}

	if($startPosition != ""){
		if($startPosition == 0){
			$sql .= " LIMIT 5";
		}
		else{
			$sql .= " LIMIT $startPosition,5";
		}
	}

   try{
        $db = new db();
        $db = $db->connect();

        $stmt = $db->query($sql);
        $sections = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
		    $rowCount = $stmt->rowCount();
		if($rowCount > 0){
			return $response->withJSON(['data' => $sections,'rowCount' => $rowCount],200,JSON_UNESCAPED_UNICODE);
		}
		else{
			return $response->withJSON(['message' => 'No content','status' => '204','rowCount' => $rowCount],204,JSON_UNESCAPED_UNICODE);
		}

     } catch(PDOException $e){
		return $response->withJSON(
			['error' => 'Internal server error',
			'system_error' => $e->getMessage(),
			'response' => '500','sql'=>$sql],
			500,
			JSON_UNESCAPED_UNICODE);
     }
});

//Get branch count
$app->get('/api/section/count', function (Request $request, Response $response, array $args) {
   $sql = "SELECT COUNT(*) AS 'count' FROM section WHERE status NOT IN (-1)";

   try{
        $db = new db();
        $db = $db->connect();

        $stmt = $db->query($sql);
        $sectionsCount = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

		if($stmt->rowCount() > 0){
			return $response->withJSON(['count' => $sectionsCount[0]->count],200,JSON_UNESCAPED_UNICODE);
		}
		else{
			return $response->withJSON(['message' => 'No content','status' => '204'],204,JSON_UNESCAPED_UNICODE);
		}

     } catch(PDOException $e){
		return $response->withJSON(
			['error' => 'Internal server error',
			'system_error' => $e->getMessage(),
			'response' => '500'],
			500,
			JSON_UNESCAPED_UNICODE);
     }

});

// Get one section

$app->get('/api/section/{id}', function (Request $request, Response $response, array $args) {
    $id = $request->getAttribute('id');
    $sql = "SELECT * FROM section WHERE sectionId = $id";

   try{
        $db = new db();
        $db = $db->connect();

        $stmt = $db->query($sql);
        if ($stmt->rowCount() > 0) {
            $section = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;
			      return $response->withJSON(['data' => $section,'status' => '200'],200,JSON_UNESCAPED_UNICODE);
        } else {
			       return $response->withJSON(['error' => 'section is not found','status' => '404'],404,JSON_UNESCAPED_UNICODE);
        }

    } catch(PDOException $e){
		return $response->withJSON(
			['error' => 'Internal server error',
			'system_error' => $e->getMessage(),
			'response' => '500'],
			500,
			JSON_UNESCAPED_UNICODE);
    }

});

// Add section

$app->post('/api/section', function (Request $request, Response $response, array $args) {
    $branchId = $request->getParam('branchId');
    $sectionName = $request->getParam('sectionName');
	  $sectionStatus = ($request->getParam('status') == null) ? $section = 0 : $section = $request->getParam('status');

    $sql = "INSERT INTO section(sectionName,branchId,status) VALUES (:sectionName,:branchId,:status)";

   try{
        $db = new db();
        $db = $db->connect();

        $stmt = $db->prepare($sql);

        $stmt->bindParam(':sectionName', $sectionName);
        $stmt->bindParam(':branchId', $branchId);
		    $stmt->bindParam(':status', $sectionStatus);

        $stmt->execute();

		    $db = null;
		    return $response->withJSON(['message' => 'section added successfully'],203,JSON_UNESCAPED_UNICODE);

      } catch(PDOException $e){
    		  return $response->withJSON(
    			['error' => 'Internal server error',
    			'system_error' => $e->getMessage(),
    			'response' => '500'],
    			500,
    			JSON_UNESCAPED_UNICODE);
      }

});

//Update section

$app->put('/api/section/{id}', function (Request $request, Response $response, array $args) {
    $id = $request->getAttribute('id');
    $branchId = $request->getParam('branchId');
    $sectionName = $request->getParam('sectionName');
		$status = $request->getParam('status');

   try{
        $db = new db();
        $db = $db->connect();

        $sqlCheck = "SELECT * FROM section WHERE sectionId = $id";

        $stmt = $db->query($sqlCheck);

        if ($stmt->rowCount() > 0) {
					$sql = "UPDATE section SET ";

					if($branchId != null & $sectionName != null){
						$sql .= "sectionName = :sectionName,branchId = :branchId";

						if($status != null){
							$sql .= ",";
						}
					}

					if($status != null){
						$sql .= "status = :status";
					}

					$sql .= " WHERE sectionId = $id";

          $stmt = $db->prepare($sql);

					if($branchId != null & $sectionName != null){
						$stmt->bindParam(':branchId', $branchId);
						$stmt->bindParam(':sectionName', $sectionName);
					}

					if($status != null){
						$stmt->bindParam(':status', $status);
					}

		      $stmt->execute();
					return $response->withJSON(["status"=> "202","success"=> "section updated"],202,JSON_UNESCAPED_UNICODE);

        } else {
						return $response->withJSON(
							['error' => 'Object not found',
			 				 'response' => '404'],
								404,
								JSON_UNESCAPED_UNICODE);
			        }

    } catch(PDOException $e){
		return $response->withJSON(
			['error' => 'Internal server error',
			'system_error' => $e->getMessage(),
			'response' => '500'],
			500,
			JSON_UNESCAPED_UNICODE);
    }

});

// Delete section

$app->delete('/api/section/{id}', function (Request $request, Response $response, array $args) {
    $id = $request->getAttribute('id');
    $sql = "DELETE FROM section WHERE section_id = $id";

   try{
        $db = new db();
        $db = $db->connect();

        $sqlCheck = "SELECT * FROM section WHERE sectionId = $id";

        $stmt = $db->query($sqlCheck);

        if ($stmt->rowCount() > 0) {

            $stmt = $db->prepare($sql);
            $stmt->execute();
            $db = null;
			return $response->withJSON(['message' => 'section deleted'],200,JSON_UNESCAPED_UNICODE);
        }else {
			       return $response->withJSON(['message' => 'section not found','status'=>'404'],404,JSON_UNESCAPED_UNICODE);
        }

    } catch(PDOException $e){
		return $response->withJSON(
			['error' => 'Internal server error',
			'system_error' => $e->getMessage(),
			'response' => '500'],
			500,
			JSON_UNESCAPED_UNICODE);
    }
});
