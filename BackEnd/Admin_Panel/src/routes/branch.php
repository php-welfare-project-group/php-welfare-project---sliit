<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

// Get all braches

$app->get('/api/branch', function (Request $request, Response $response, array $args) {
	$startPosition = $request->getHeaderLine('startPosition');
	$active = $request->getHeaderLine('active');
	$sql = "SELECT branch.branchId, branch.deptID, branch.name, branch.status, department.deptName
			FROM branch
			LEFT JOIN department ON branch.deptID = department.deptID ";

	if($active == ""){
		$sql .=	"WHERE branch.status != -1";
	}
	else{
		$sql .= "WHERE branch.status = 1";
	}
	

			if($startPosition != ""){
				if($startPosition == 0){
					$sql .= " LIMIT 5";
				}
				else{
					$sql .= " LIMIT $startPosition,5";
				}
			}

   try{
        $db = new db();
        $db = $db->connect();

        $stmt = $db->query($sql);
        $branches = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;
				$rowCount = $stmt->rowCount();

				/*
				Access-Control-Allow-Origin: http://webdavserver.com
Access-Control-Allow-Credentials: true
Access-Control-Allow-Methods: ACL, CANCELUPLOAD, CHECKIN, CHECKOUT, COPY, DELETE, GET, HEAD, LOCK, MKCALENDAR, MKCOL, MOVE, OPTIONS, POST, PROPFIND, PROPPATCH, PUT, REPORT, SEARCH, UNCHECKOUT, UNLOCK, UPDATE, VERSION-CONTROL
Access-Control-Allow-Headers: Overwrite, Destination, Content-Type, Depth, User-Agent, Translate, Range, Content-Range, Timeout, X-File-Size, X-Requested-With, If-Modified-Since, X-File-Name, Cache-Control, Location, Lock-Token, If
Access-Control-Expose-Headers: DAV, content-length, Allow
				*/
				$response = $response->withAddedHeader('Access-Control-Allow-Origin', 'http://localhost:3000/dashboard/branch/list');
				$response = $response->withAddedHeader('Content-Type','application/json');
				// $response = $response->withAddedHeader('Access-Control-Allow-Credentials', 'true');
				$response = $response->withAddedHeader('Access-Control-Allow-Methods', 'POST, GET, DELETE, PUT, PATCH, OPTIONS');
				$response = $response->withAddedHeader('Access-Control-Allow-Headers', 'token, Content-Type');
				// $response = $response->withAddedHeader('Access-Control-Expose-Headers', 'DAV, content-length, Allow');

		if($rowCount > 0){
			return $response->withJSON(['data' => $branches,'rowCount' => $rowCount, 'Headers' => $response->getHeaders(),'Origin' => $_SERVER['HTTP_ORIGIN']],200,JSON_UNESCAPED_UNICODE);
		}
		else{
			return $response->withJSON(['message' => 'No content','status' => '204','rowCount' => $rowCount],204,JSON_UNESCAPED_UNICODE);
		}

     } catch(PDOException $e){
		return $response->withJSON(
			['error' => 'Internal server error',
			'system_error' => $e->getMessage(),
			'response' => '500'],
			500,
			JSON_UNESCAPED_UNICODE);
     }
});

//Get branch count
$app->get('/api/branch/count', function (Request $request, Response $response, array $args) {
   $sql = "SELECT COUNT(*) AS 'count' FROM branch WHERE status NOT IN (-1)";

   try{
        $db = new db();
        $db = $db->connect();

        $stmt = $db->query($sql);
        $branchesCount = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

		if($stmt->rowCount() > 0){
			return $response->withJSON(['count' => $branchesCount[0]->count],200,JSON_UNESCAPED_UNICODE);
		}
		else{
			return $response->withJSON(['message' => 'No content','status' => '204'],204,JSON_UNESCAPED_UNICODE);
		}

     } catch(PDOException $e){
		return $response->withJSON(
			['error' => 'Internal server error',
			'system_error' => $e->getMessage(),
			'response' => '500'],
			500,
			JSON_UNESCAPED_UNICODE);
     }

});

// Get one braches

$app->get('/api/branch/{id}', function (Request $request, Response $response, array $args) {
    $id = $request->getAttribute('id');
    $sql = "SELECT * FROM branch WHERE branchId = $id";

   try{
        $db = new db();
        $db = $db->connect();

        $stmt = $db->query($sql);
        if ($stmt->rowCount() > 0) {
            $branch = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;
						return $response->withJSON(
						['data' => $branch,'status' => '200'],200,JSON_UNESCAPED_UNICODE);

        } else {
						return $response->withJSON(
							['error' => 'Object not found',
			 				 'response' => '404'],
								404,
								JSON_UNESCAPED_UNICODE);
        }

    } catch(PDOException $e){
				return $response->withJSON(
					['error' => 'Internal server error',
					'system_error' => $e->getMessage(),
					'response' => '500'],
					500,
					JSON_UNESCAPED_UNICODE);
		    }

});

// Add branch

$app->post('/api/branch', function (Request $request, Response $response, array $args) {
    $deptId = $request->getParam('deptID');
    $branchName = $request->getParam('name');
    $status = $request->getParam('status');

	$sql = "INSERT INTO branch(name, deptID, status) VALUES (:name,:deptID,:states)";

   try{
        $db = new db();
        $db = $db->connect();

        $stmt = $db->prepare($sql);

        $stmt->bindParam(':deptID', $deptId);
        $stmt->bindParam(':name', $branchName);
        $stmt->bindParam(':states', $status);

        $stmt->execute();

				return $response->withJSON(["status" => "203","message" => "branch added"],203,JSON_UNESCAPED_UNICODE);

    } catch(PDOException $e){
		return $response->withJSON(
			['error' => 'Internal server error',
			'system_error' => $e->getMessage(),
			'response' => '500'],
			500,
			JSON_UNESCAPED_UNICODE);
    }

});

//Update branch

$app->put('/api/branch/{id}', function (Request $request, Response $response, array $args) {
    $id = $request->getAttribute('id');
    $deptID = $request->getParam('deptID');
    $branchName = $request->getParam('name');
		$status = $request->getParam('status');

   try{
        $db = new db();
        $db = $db->connect();

        $sqlCheck = "SELECT * FROM branch WHERE branchId = $id";

        $stmt = $db->query($sqlCheck);

        if ($stmt->rowCount() > 0) {
					$sql = "UPDATE branch SET ";

					if($deptID != null & $branchName != null){
						$sql .= "name = :name,deptID = :deptID";

						if($status != null){
							$sql .= ",";
						}
					}

					if($status != null){
						$sql .= "status = :status";
					}

					$sql .= " WHERE branchId = $id";

          $stmt = $db->prepare($sql);

					if($deptID != null & $branchName != null){
						$stmt->bindParam(':deptID', $deptID);
						$stmt->bindParam(':name', $branchName);
					}

					if($status != null){
						$stmt->bindParam(':status', $status);
					}

		      $stmt->execute();
					return $response->withJSON(["status"=> "202","success"=> "branch updated"],202,JSON_UNESCAPED_UNICODE);

        } else {
						return $response->withJSON(
							['error' => 'Object not found',
			 				 'response' => '404'],
								404,
								JSON_UNESCAPED_UNICODE);
			        }

    } catch(PDOException $e){
		return $response->withJSON(
			['error' => 'Internal server error',
			'system_error' => $e->getMessage(),
			'response' => '500'],
			500,
			JSON_UNESCAPED_UNICODE);
    }

});

// Delete brach

$app->delete('/api/branch/{id}', function (Request $request, Response $response, array $args) {
    $id = $request->getAttribute('id');
    $sql = "DELETE FROM branch WHERE branchId = $id";

   try{
        $db = new db();
        $db = $db->connect();

        $sqlCheck = "SELECT * FROM branch WHERE branchId = $id";

        $stmt = $db->query($sqlCheck);

        if ($stmt->rowCount() > 0) {

            $stmt = $db->prepare($sql);
            $stmt->execute();
            $db = null;
						return $response->withJSON(["status"=>"200","success"=>"branch deleted"],200,JSON_UNESCAPED_UNICODE);
        } else {
			return $response->withJSON(
				['error' => 'Object not found',
 				 'response' => '404'],
					404,
					JSON_UNESCAPED_UNICODE);
        }

    } catch(PDOException $e){
		return $response->withJSON(
			['error' => 'Internal server error',
			'system_error' => $e->getMessage(),
			'response' => '500'],
			500,
			JSON_UNESCAPED_UNICODE);
    }

});
