<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;


//Get All Accounts
$app->get('/api/accounts', function(Request $request, Response $response){

    $startPosition = $request->getHeaderLine('startPosition');

    $sql = "SELECT accounts.accountid, accounts.eid, accounts.username, accounts.password, accounts.token, accounts.expirydatetime, accounts.registereddatetime
			FROM accounts
            JOIN employees ON accounts.eid = employees.eid
			WHERE employees.status != -1";

			if($startPosition != ""){
				if($startPosition == 0){
					$sql .= " LIMIT 5";
				}
				else{
					$sql .= " LIMIT $startPosition,5";
				}
            }



    //$sql = "SELECT * FROM accounts";

    try {
        //Get DB Object
        $db = new db();
        //Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $accounts = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

        $rowCount = $stmt->rowCount();
		if($rowCount > 0){
			return $response->withJSON(['data' => $accounts,'rowCount' => $rowCount],200,JSON_UNESCAPED_UNICODE);
		}
		else{
			return $response->withJSON(['message' => 'No content','status' => '204','rowCount' => $rowCount],204,JSON_UNESCAPED_UNICODE);
		}


        //echo json_encode($accounts);

    } catch (PDOException $e) {
        return $response->withJSON(
			['error' => 'Internal server error',
			'system_error' => $e->getMessage(),
			'response' => '500'],
			500,
			JSON_UNESCAPED_UNICODE);
    }
});


//Get Single Account
$app->get('/api/accounts/{id}', function(Request $request, Response $response){

    //Get by accountid
    $accountid = $request->getAttribute('id');
    $sql = "SELECT * FROM accounts WHERE accountid = '$accountid' ";

    try {
        //Get DB Object
        $db = new db();
        //Connect
        $db = $db->connect();

        $stmt = $db->query($sql);

        $account = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

        $rowCount = $stmt->rowCount();
		if($rowCount > 0){
			return $response->withJSON(['data' => $account,'rowCount' => $rowCount],200,JSON_UNESCAPED_UNICODE);
		}
		else{
			return $response->withJSON(['message' => 'Not Found','status' => '404','rowCount' => $rowCount],204,JSON_UNESCAPED_UNICODE);
		}

        //echo json_encode($account);

    } catch (PDOException $e) {
        return $response->withJSON(
			['error' => 'Internal server error',
			'system_error' => $e->getMessage(),
			'response' => '500'],
			500,
			JSON_UNESCAPED_UNICODE);
    }
});


//Add account
$app->post('/api/accounts', function(Request $request, Response $response){

    $empId = $request->getParam('empId');
    $userName = $request->getParam('userName');
    $password = $request->getParam('password');
    $role = $request->getParam('role');

    // $token = $request->getParam('token');  //Can be Null
    // $expirydatetime = $request->getParam('expirydatetime'); //Can be Null
    // //Making the token and expirydatetime null
    // $token = null;
    // $expirydatetime = null;
    //
    // $registereddatetime = $request->getParam('registereddatetime'); //Can be Null

    $algo = "sha256";
    $hashpassword = hash ($algo, $password, false);

    $sql = "INSERT INTO account (empId,userName,password,role) VALUES (:empId,:userName,:hashpassword,:role)";

    try {
        //Get DB Object
        $db = new db();
        //Connect
        $db = $db->connect();

        $stmt = $db->prepare($sql);

        $stmt->bindParam(':empId',$empId);
        $stmt->bindParam(':userName',$userName);
        $stmt->bindParam(':hashpassword',$hashpassword);
        $stmt->bindParam(':role',$role);
        // $stmt->bindParam(':token',$token);
        // $stmt->bindParam(':expirydatetime',$expirydatetime);
        // $stmt->bindParam(':registereddatetime',$registereddatetime);

        $stmt->execute();

        $db = null;
        return $response->withJSON(["status" => "203","message" => "account added"],203,JSON_UNESCAPED_UNICODE);

        //echo '{"notice": {"text": "Account Added"}"}';

    } catch (PDOException $e) {
        return $response->withJSON(
			['error' => 'Internal server error',
			'system_error' => $e->getMessage(),
			'response' => '500'],
			500,
			JSON_UNESCAPED_UNICODE);
    }

});


//Update Account Password
$app->put('/api/accounts/{id}', function(Request $request, Response $response){

    $eid = $request->getAttribute('id');
    //$eid = $request->getParam('eid');
    $username = $request->getParam('username');
    $password = $request->getParam('password');
    $newpassword = $request->getParam('newpassword');


    $algo = "sha256";
    $hashpassword = hash ($algo, $password, false);
    $hashnewpassword = hash ($algo, $newpassword, false);
    $dbusername = '';
    $dbhashpassword = '';

    $sql = "SELECT username,password FROM accounts WHERE eid = '$eid'";

    try {
        //Get DB Object
        $db = new db();
        //Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        $db = null;

        if($result == false){
            return $response->withJSON(['message' => 'Not Found','status' => '404'],404,JSON_UNESCAPED_UNICODE);
        }
        else{
            $dbusername = $result['username'];
            $dbhashpassword = $result['password'];
        }

    } catch (PDOException $e) {
        return $response->withJSON(
			['error' => 'Internal server error',
			'system_error' => $e->getMessage(),
			'response' => '500'],
			500,
			JSON_UNESCAPED_UNICODE);
    }

    if($dbusername===$username && $dbhashpassword===$hashpassword){

        $sql = "UPDATE accounts SET
            password = :hashnewpassword
        WHERE eid = '$eid'";

        try {
            //Get DB Object
            $db = new db();
            //Connect
            $db = $db->connect();

            $stmt = $db->prepare($sql);

            //$stmt->bindParam(':eid',$eid);
            $stmt->bindParam(':hashnewpassword',$hashnewpassword);

            $stmt->execute();
            $db = null;

            return $response->withJSON(["status"=> "202","success"=> "password updated",'sql' => $sql],202,JSON_UNESCAPED_UNICODE);

        } catch (PDOException $e) {
            return $response->withJSON(['error' => $e->getMessage(),'status' => '500','sql'->$sql],500,JSON_UNESCAPED_UNICODE);
        }

    }
    else{
        $invalidcredetials = "Invalid Credetials";
        return $response->withJSON(['error' => 'Invalid Credentials','status'=> '401','sql' => $sql],401,JSON_UNESCAPED_UNICODE);
    }

});

//Update role
$app->put('/api/accountrole/{id}', function(Request $request, Response $response, array $args){
    $empId = $request->getAttribute('id');
    $role = $request->getParam('role');

    $sqlCheck = "SELECT * FROM account WHERE empId = '$empId'";

    try{
        $db = new db();
        $db = $db->connect();

        $stmt = $db->query($sqlCheck);
        $result = $stmt->fetch(PDO::FETCH_OBJ);

        if($stmt->rowCount() > 0){
            $sql = "UPDATE account SET role = :role WHERE empId = '$empId'";

            $stmt = $db->prepare($sql);
            $stmt->bindParam(':role', $role);
            $stmt->execute();

            // $db = null

            return $response->withJSON(["status"=> "202","success"=> "branch updated"],202,JSON_UNESCAPED_UNICODE);
        }
        else {
            return $response->withJSON(['message' => 'Not Found','status' => '404'],404,JSON_UNESCAPED_UNICODE);
        }
    }catch (PDOException $e) {
        return $response->withJSON(
			['error' => 'Internal server error',
			'system_error' => $e->getMessage(),
			'response' => '500'],
			500,
			JSON_UNESCAPED_UNICODE);
    }
});


// //Delete/Activate/Deactivate Account
// $app->put('/api/accounts/{id}', function(Request $request, Response $response){

//     $eid = $request->getAttribute('id');
//     $status = $request->getAttribute('status');//Status = 0(Deactive),1(Active),-1(Deleted)
//     //$sql = "DELETE FROM accounts WHERE eid = $eid"; Delete Permently From the DB
//     $sql = "UPDATE accounts SET
//             status = :status
//     WHERE eid = $eid";


//     try {
//         //Get DB Object
//         $db = new db();
//         //Connect
//         $db = $db->connect();

//         $stmt = $db->prepare($sql);

//         $stmt->bindParam(':status',$status);//For Deleting by status = -1

//         $stmt->execute();

//         echo '{"notice": {"text": "Account Deleted"}"}';

//         $db = null;

//     } catch (PDOException $e) {
//         echo '{"error": {"text": '.$e->getMessage().'}}';
//     }
// });
