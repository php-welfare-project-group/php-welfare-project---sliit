<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

//Get All Employees
$app->get('/api/employees', function(Request $request, Response $response, array $args){
  $startPosition = $request->getHeaderLine('startPosition');
  $sectionId = $request->getHeaderLine('sectionId');

  if($sectionId !=""){

  	$sql = "SELECT employee.empId, employee.fName, employee.lName, employee.designation, employee.status
  					FROM ((employee
  					INNER JOIN worksin ON employee.empId = worksin.empId)
            INNER JOIN section ON worksin.sectionId = section.sectionId)
  					WHERE employee.status != -1 AND worksin.sectionId =$sectionId";
  }
  else {
    $sql = "SELECT employee.*, account.role FROM employee
            LEFT JOIN account ON employee.empId = account.empId
            WHERE employee.status != -1";
  }

			if($startPosition != ""){
				if($startPosition == 0){
					$sql .= " LIMIT 5";
				}
				else{
					$sql .= " LIMIT $startPosition,5";
				}
			}

    try {
        //Get DB Object
        $db = new db();
        //Connect
        $db = $db->connect();

        $stmt = $db->query($sql);
        $employees = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

        $rowCount = $stmt->rowCount();
		if($rowCount > 0){
			return $response->withJSON(['data' => $employees,'rowCount' => $rowCount],200,JSON_UNESCAPED_UNICODE);
		}
		else{
			return $response->withJSON(['message' => 'No content','status' => '204','rowCount' => $rowCount],204,JSON_UNESCAPED_UNICODE);
		}

    // echo json_encode($employees);

    } catch (PDOException $e) {
        return $response->withJSON(
			['error' => 'Internal server error',
			'system_error' => $e->getMessage(),
			'response' => '500'],
			500,
			JSON_UNESCAPED_UNICODE);
    }
});

//Get COUNT
$app->get('/api/employees/count', function (Request $request, Response $response, array $args) {
  $sql = "SELECT COUNT(*) AS 'count' FROM employee WHERE status NOT IN (-1)";

  try{
       $db = new db();
       $db = $db->connect();

       $stmt = $db->query($sql);
       $employeeCount = $stmt->fetchAll(PDO::FETCH_OBJ);
       $db = null;

   if($stmt->rowCount() > 0){
     return $response->withJSON(['count' => $employeeCount[0]->count],200,JSON_UNESCAPED_UNICODE);
   }
   else{
     return $response->withJSON(['message' => 'No content','status' => '204'],204,JSON_UNESCAPED_UNICODE);
   }

    } catch(PDOException $e){
   return $response->withJSON(
     ['error' => 'Internal server error',
     'system_error' => $e->getMessage(),
     'response' => '500'],
     500,
     JSON_UNESCAPED_UNICODE);
    }

});

//Get Single Employee
$app->get('/api/employees/{id}', function(Request $request, Response $response, array $args){

    $eid = $request->getAttribute('id');
    $sql = "SELECT * FROM employee WHERE empId = '$eid' ";

    try {
        //Get DB Object
        $db = new db();
        //Connect
        $db = $db->connect();

        $stmt = $db->query($sql);

        $employee = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

        $rowCount = $stmt->rowCount();
		if($rowCount > 0){
			return $response->withJSON(['data' => $employee,'rowCount' => $rowCount],200,JSON_UNESCAPED_UNICODE);
		}
		else{
			return $response->withJSON(['message' => 'Not Found','status' => '404','rowCount' => $rowCount],404,JSON_UNESCAPED_UNICODE);
		}


    } catch (PDOException $e) {
        return $response->withJSON(
			['error' => 'Internal server error',
			'system_error' => $e->getMessage(),
			'response' => '500'],
			500,
			JSON_UNESCAPED_UNICODE);
    }
});

//Get single employee from nic
$app->get('/api/employees/nic/{nic}', function(Request $request, Response $response, array $args){
  $nic = $request->getAttribute('nic');
  $account = $request->getHeaderLine('account');
  $sql = "";

  if($account == null){
    $sql = "SELECT e.empId,e.nic,e.fName,e.lName,e.mName,e.dob,e.address,e.district,e.divisionSecretariat,e.gramaNilaDivision,
    e.designation,e.officialName,e.dateOfAppointment,e.dateOfRetirement,e.mobileNo,e.homePhone,e.whatsappNo,e.imoNo,
    e.eduQualification,e.profQualification,e.status
    FROM employee e
    WHERE nic = '$nic'";
  }
  else{
    $sql = "SELECT e.empId,e.nic,e.fName,e.lName,e.mName,e.dob,e.address,e.district,e.divisionSecretariat,e.gramaNilaDivision,
    e.designation,e.officialName,e.dateOfAppointment,e.dateOfRetirement,e.mobileNo,e.homePhone,e.whatsappNo,e.imoNo,
    e.eduQualification,e.profQualification,e.status,a.userName,a.password,a.role
    FROM employee e,account a
    WHERE nic = '$nic' AND a.empId = e.empId";
  }

  try {
      //Get DB Object
      $db = new db();
      //Connect
      $db = $db->connect();

      $stmt = $db->query($sql);

      $employee = $stmt->fetchAll(PDO::FETCH_OBJ);
      $db = null;

      $rowCount = $stmt->rowCount();
  if($rowCount > 0){
    return $response->withJSON(['data' => $employee,'rowCount' => $rowCount,'sql' => $sql],200,JSON_UNESCAPED_UNICODE);
  }
  else{
    return $response->withJSON(['message' => 'Not Found','status' => '404','rowCount' => $rowCount,'sql' => $sql],404,JSON_UNESCAPED_UNICODE);
  }

  } catch (PDOException $e) {
      return $response->withJSON(
    ['error' => 'Internal server error',
    'system_error' => $e->getMessage(),
    'response' => '500','sql' => $sql],
    500,
    JSON_UNESCAPED_UNICODE);
  }
});

$app->get('/api/employees/role/{nic}', function(Request $request, Response $response, array $args){
  $nic = $request->getAttribute('nic');

  $sql = "SELECT a.role
          FROM employee e,account a
          WHERE e.nic = '$nic' AND a.empId = e.empId";

  try {
      //Get DB Object
      $db = new db();
      //Connect
      $db = $db->connect();

      $stmt = $db->query($sql);

      $employee = $stmt->fetchAll(PDO::FETCH_OBJ);
      $db = null;

      $rowCount = $stmt->rowCount();
  if($rowCount > 0){
    return $response->withJSON(['data' => $employee,'rowCount' => $rowCount,'sql' => $sql],200,JSON_UNESCAPED_UNICODE);
  }
  else{
    return $response->withJSON(['message' => 'Not Found','status' => '404','rowCount' => $rowCount,'sql' => $sql],404,JSON_UNESCAPED_UNICODE);
  }


  } catch (PDOException $e) {
      return $response->withJSON(
    ['error' => 'Internal server error',
    'system_error' => $e->getMessage(),
    'response' => '500','sql' => $sql],
    500,
    JSON_UNESCAPED_UNICODE);
  }
});


//Add Employee
$app->post('/api/employees', function(Request $request, Response $response, array $args){

    /*
    $directory = $this->get('upload_directory');

    $uploadedFiles = $request->getUploadedFiles();
    if (empty($uploadedFiles['imagefile'])) {
        throw new Exception('Expected a imagefile');
    }
    // handle single input with single file upload
    $uploadedFile = $uploadedFiles['imagefile'];
    if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
        $filename = moveUploadedFile($directory, $uploadedFile);
        $response->write('uploaded ' . $filename . '<br/>');
    }

    function moveUploadedFile($directory, UploadedFile $uploadedFile)
    {
        $extension = pathinfo($uploadedFile->getClientFilename(), PATHINFO_EXTENSION);
        $basename = bin2hex(random_bytes(8)); // see http://php.net/manual/en/function.random-bytes.php
        $filename = sprintf('%s.%0.8s', $basename, $extension);

        $uploadedFile->moveTo($directory . DIRECTORY_SEPARATOR . $filename);

        return $filename;
    }

    */

    $eid = generateeid();

    $nic = $request->getParam('nic');
    $fName = $request->getParam('fName');
    $mName = $request->getParam('mName');
    $lName = $request->getParam('lName');
    $dob = $request->getParam('dob');
    $address = $request->getParam('address');
    $district = $request->getParam('district');
    $divisionSecretariat = $request->getParam('divisionSecretariat');
    $gramaNilaDivision = $request->getParam('gramaNilaDivision');
    $designation = $request->getParam('designation');
    $officialName = $request->getParam('officialName');
    $dateOfAppointment = $request->getParam('dateOfAppointment');
    $dateOfRetirement = $request->getParam('dateOfRetirement');
    $mobileNo = $request->getParam('mobileNo');
    $homePhone = $request->getParam('homePhone');
    $whatsappNo = $request->getParam('whatsappNo');
    $imoNo = $request->getParam('imoNo');
    $eduQualification = $request->getParam('eduQualification');
    $profQualification = $request->getParam('profQualification');
    $status = $request->getParam('status');

    $sql = "INSERT INTO employee (nic,fName,mName,lName,dob,address,district,divisionSecretariat,gramaNilaDivision,designation,officialName,
            dateOfAppointment,dateOfRetirement,mobileNo,homePhone,whatsappNo,imoNo,eduQualification,profQualification,status) VALUES
            (:nic,:fName,:mName,:lName,:dob,:address,:district,:divisionSecretariat,:gramaNilaDivision,:designation,:officialName,
             :dateOfAppointment,:dateOfRetirement,:mobileNo,:homePhone,:whatsappNo,:imoNo,:eduQualification,:profQualification,:status)";

    try {
        //Get DB Object
        $db = new db();
        //Connect
        $db = $db->connect();

        $stmt = $db->prepare($sql);

        $stmt->bindParam(':nic', $nic);
        $stmt->bindParam(':fName', $fName);
        $stmt->bindParam(':mName', $mName);
        $stmt->bindParam(':lName', $lName);
        $stmt->bindParam(':dob', $dob);
        $stmt->bindParam(':address', $address);
        $stmt->bindParam(':district', $district);
        $stmt->bindParam(':divisionSecretariat', $divisionSecretariat);
        $stmt->bindParam(':gramaNilaDivision', $gramaNilaDivision);
        $stmt->bindParam(':designation', $designation);
        $stmt->bindParam(':officialName', $officialName);
        $stmt->bindParam(':dateOfAppointment', $dateOfAppointment);
        $stmt->bindParam(':dateOfRetirement', $dateOfRetirement);
        $stmt->bindParam(':mobileNo', $mobileNo);
        $stmt->bindParam(':homePhone', $homePhone);
        $stmt->bindParam(':whatsappNo', $whatsappNo);
        $stmt->bindParam(':imoNo', $imoNo);
        $stmt->bindParam(':eduQualification', $eduQualification);
        $stmt->bindParam(':profQualification', $profQualification);
        $stmt->bindParam(':status', $status);


        $stmt->execute();

        $db = null;
        return $response->withJSON(["status" => "203","message" => "employee added"],203,JSON_UNESCAPED_UNICODE);

    } catch (PDOException $e) {
        return $response->withJSON(
			['error' => 'Internal server error',
			'system_error' => $e->getMessage(),
			'response' => '500'],
			500,
			JSON_UNESCAPED_UNICODE);
    }

});

//Update Employee

$app->put('/api/employees/{id}', function (Request $request, Response $response, array $args) {
    $id = $request->getAttribute('id');
    $nic = $request->getParam('nic');
    $fName = $request->getParam('fName');
    $mName = $request->getParam('mName');
    $lName = $request->getParam('lName');
    $dob = $request->getParam('dob');
    $address = $request->getParam('address');
    $district = $request->getParam('district');
    $divisionSecretariat = $request->getParam('divisionSecretariat');
    $gramaNilaDivision = $request->getParam('gramaNilaDivision');
    $designation = $request->getParam('designation');
    $officialName = $request->getParam('officialName');
    $dateOfAppointment = $request->getParam('dateOfAppointment');
    $dateOfRetirement = $request->getParam('dateOfRetirement');
    $mobileNo = $request->getParam('mobileNo');
    $homePhone = $request->getParam('homePhone');
    $whatsappNo = $request->getParam('whatsappNo');
    $imoNo = $request->getParam('imoNo');
    $eduQualification = $request->getParam('eduQualification');
    $profQualification = $request->getParam('profQualification');
    $status = $request->getParam('status');

    try{
      $db = new db();
      $db = $db->connect();

      $sqlCheck = "SELECT * FROM employee WHERE empId = $id";

      $stmt = $db->query($sqlCheck);
      $rowCount = $stmt->rowCount();

      if ($rowCount > 0){
        $sql = "UPDATE employee SET ";

        if ($nic != null & $fName !=null ) {
            $sql .= "nic = :nic,
                     fName = :fName,
                     mName = :mName,
                     lName = :lName,
                     dob = :dob,
                     address = :address,
                     district = :district,
                     divisionSecretariat = :divisionSecretariat,
                     gramaNilaDivision = :gramaNilaDivision,
                     designation = :designation,
                     officialName = :officialName,
                     dateOfAppointment = :dateOfAppointment,
                     dateOfRetirement = :dateOfRetirement,
                     mobileNo = :mobileNo,
                     homePhone = :homePhone,
                     whatsappNo = :whatsappNo,
                     imoNo = :imoNo,
                     eduQualification = :eduQualification,
                     profQualification = :profQualification";

            if($status != null){
              $sql .= ",";
            }
        }
        if ($status != null){
          $sql .= "status = :status";
        }
        $sql .= " WHERE empId = $id";

        $stmt = $db->prepare($sql);

        if($nic != null & $fName != null){
          $stmt->bindParam(':nic', $nic);
          $stmt->bindParam(':fName', $fName);
          $stmt->bindParam(':mName', $mName);
          $stmt->bindParam(':lName', $lName);
          $stmt->bindParam(':dob', $dob);
          $stmt->bindParam(':address', $address);
          $stmt->bindParam(':district', $district);
          $stmt->bindParam(':divisionSecretariat', $divisionSecretariat);
          $stmt->bindParam(':gramaNilaDivision', $gramaNilaDivision);
          $stmt->bindParam(':designation', $designation);
          $stmt->bindParam(':officialName', $officialName);
          $stmt->bindParam(':dateOfAppointment', $dateOfAppointment);
          $stmt->bindParam(':dateOfRetirement', $dateOfRetirement);
          $stmt->bindParam(':mobileNo', $mobileNo);
          $stmt->bindParam(':homePhone', $homePhone);
          $stmt->bindParam(':whatsappNo', $whatsappNo);
          $stmt->bindParam(':imoNo', $imoNo);
          $stmt->bindParam(':eduQualification', $eduQualification);
          $stmt->bindParam(':profQualification', $profQualification);
        }

        if($status != null){
          $stmt->bindParam(':status', $status);
        }

        $stmt->execute();
        $db = null;

        return $response->withJSON(['message' => 'Employee updated', 'status' => '202'],202,JSON_UNESCAPED_UNICODE);

      }
      else {
        return $response->withJSON(['message' => 'Employee not found', 'status' => '404'],404,JSON_UNESCAPED_UNICODE);
      }
    }
    catch(PDOException $e){
      return $response->withJSON(['error' => 'Internel server error',
                                  'system_error' => $e->getMessage(),
                                  'response' => '500'],
                                  500,
                                  JSON_UNESCAPED_UNICODE);
    }
});

// Delete employee

$app->delete('/api/employees/{id}', function (Request $request, Response $response, array $args) {
    $id = $request->getAttribute('id');

    $sql = "DELETE FROM employee WHERE empId = $id";

    try{
      $db = new db();
      $db = $db->connect();

      $sqlCheck = "SELECT * FROM employee WHERE empId = $id";
      $stmt = $db->query($sqlCheck);

      if($stmt->rowCount() > 0) {
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $db = null;

        return $response->withJSON(['message' => 'Employee deleted', 'status' => '200'],200,JSON_UNESCAPED_UNICODE);
      }
      else {
        return $response->withJSON(['message' => 'Employee not found', 'status' => '404'],404,JSON_UNESCAPED_UNICODE);
      }
    }
    catch(PDOException $e){
      return $response->withJSON(['error' => 'Interal server error',
                                  'system_error' => $e->getMessage(),
                                  'response' => '500'],
                                  500,
                                  JSON_UNESCAPED_UNICODE);
    }


});

//Generating a eid

//19-345678-HR

function generateeid() {

    $file = '../src/routes/middleid.txt';

    //get the number from the file
    $uniq = file_get_contents($file);
    //add +1
    $midid = $uniq + 1 ;

    // add that new value to text file again for next use
    file_put_contents($file, $midid);

    $now = new DateTime();
    $year = $now->format("Y");
    $yearid = (string)$year[2].(string)$year[3];
    $ministryid = "HR";
    $generatedeid = $yearid.$midid.$ministryid;
    return $generatedeid;
}

function checkNull($value,$sql,$fieldName,$commaFlag){
    //$sqlParts = $sql.explode(',');

    if($value !== null){
        if($commaFlag){
            $sql .= $fieldName.' = "'.$value.'",';
        }
        else{
            $sql .= $fieldName.' = "'.$value.'"';
        }

        return $sql;
    }
}
