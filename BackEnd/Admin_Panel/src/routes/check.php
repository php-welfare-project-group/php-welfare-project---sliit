<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/api/check/section/{id}', function (Request $request, Response $response, array $args) {
    $id = $request->getAttribute('id');
    $sql = "SELECT employee.status
            FROM ((section
            INNER JOIN worksin ON section.sectionId = worksin.sectionId)
            INNER JOIN employee ON worksin.empId = employee.empId)
            WHERE section.sectionId = $id";

   try{
        $db = new db();
        $db = $db->connect();

        $stmt = $db->query($sql);
        $rowCount = $stmt->rowCount();
        if ($rowCount > 0) {
            $section = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;
			return $response->withJSON(['data' => $section, 'rowCount' => $rowCount, 'status' => '200'],200,JSON_UNESCAPED_UNICODE);
        } else {
			return $response->withJSON(['error' => 'section is not found','status' => '204'],204,JSON_UNESCAPED_UNICODE);
        }

    } catch(PDOException $e){
		return $response->withJSON(
			['error' => 'Internal server error',
			'system_error' => $e->getMessage(),
			'response' => '500'],
			500,
			JSON_UNESCAPED_UNICODE);

    }

});

$app->get('/api/check/branch/{id}', function (Request $request, Response $response, array $args) {
    $id = $request->getAttribute('id');
    $sql = "SELECT section.status
            FROM branch
            INNER JOIN section ON branch.branchId = section.branchId
            WHERE branch.branchId = $id";

   try{
        $db = new db();
        $db = $db->connect();

        $stmt = $db->query($sql);
        $rowCount = $stmt->rowCount();
        if ($rowCount > 0) {
            $section = $stmt->fetchAll(PDO::FETCH_OBJ);
            $db = null;
			return $response->withJSON(['data' => $section, 'rowCount' => $rowCount, 'status' => '200'],200,JSON_UNESCAPED_UNICODE);
        } else {
			return $response->withJSON(['error' => 'section is not found','status' => '204'],204,JSON_UNESCAPED_UNICODE);
        }

    } catch(PDOException $e){
		return $response->withJSON(
			['error' => 'Internal server error',
			'system_error' => $e->getMessage(),
			'response' => '500'],
			500,
			JSON_UNESCAPED_UNICODE);

    }

});
