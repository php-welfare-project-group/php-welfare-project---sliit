<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';
require '../src/config/db.php';

$app = new \Slim\App;
$app->get('/hello/{name}', function (Request $request, Response $response, array $args) {
    $name = $args['name'];
    $response->getBody()->write("Hello, $name");

    return $response;
});

// Branch Routes

require '../src/routes/section.php';
require '../src/routes/branch.php';
require '../src/routes/check.php';
require '../src/routes/department.php';
require '../src/routes/worksin.php';

//Employees Routes
require '../src/routes/employees.php';
require '../src/routes/accounts.php';
//require '../src/routes/profilepictureupload.php';

$app->run();
