import React from 'react'
import Header from "./Header"
import SideBar from "./SideBar/SideBar"
import './DashBoard.css'
import Profile from "../Profile/Profile";

export default class DashBoard extends React.Component {
    componentWillMount = () => {
        localStorage.setItem('logged_in', true)
        localStorage.setItem('live_sub_menu', this.props.activeSubMenu.split('_')[0])
        console.log(this.props)
    }

    render = () => {
        return (
            <div style={{height:"100%"}}>
                <Header setLoggedIn={this.props.loggedIn}/>
                <div style={{display:"flex",minHeight:"91.5vh"}}>
                    <div style={{display:"inline-flex",paddingLeft:"0px",paddingRight:"0px"}} className="col-md-2">
                        <SideBar activeSubMenu={this.props.activeSubMenu}/>
                    </div>
                    <div className="col-md-10" style={{display:"inline-flex",padding:"0px",height:"91.5vh"}}>
                        <div className="col-md-12" style={{padding:"10px",overflowY:"scroll"}}>
                            {this.props.body}
                        </div>
                    </div>
                </div>
                <Profile/>
            </div>
        )
    }
}