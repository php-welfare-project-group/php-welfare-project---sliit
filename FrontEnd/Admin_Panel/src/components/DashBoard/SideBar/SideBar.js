import React from 'react'
import './SideBar.css'
import {Link} from 'react-router-dom'
import SideBarSubMenu from "./SideBarSubMenu";
import sideBarConfig from '../../config/sidebar'

export default class SideBar extends React.Component {
    componentDidMount = () => {
        console.log(this.props)
        let subMenuID = this.props.activeSubMenu.split('_')[0]
        console.log(this.props.activeSubMenu.split('_')[0])
         let button = document.getElementById('button__' + subMenuID)
        console.log(button)

        if (button !== null) {
            console.log(button.className)
            if (button.className === 'fas fa-angle-left menuArrowIcon-left') {
                button.className = 'fas fa-angle-left menuArrowIcon-down'
            }
            else {
                button.className = 'fas fa-angle-left menuArrowIcon-left'
            }
        }
    }

    toggleSubMenu = ({target}) => {
        console.log(target)
        console.log('name : ' + target.id)
        let subMenuID = target.id.split('__')[1]
        let button = document.getElementById('button__' + subMenuID)

        if (button !== null) {
            console.log(button.className)
            if (button.className === 'fas fa-angle-left menuArrowIcon-left') {
                button.className = 'fas fa-angle-left menuArrowIcon-down'
            }
            else {
                button.className = 'fas fa-angle-left menuArrowIcon-left'
            }
        }
    }

    render() {
        let sideBarMenu = sideBarConfig.map((item, i) => {
            console.log(item.subs)
            console.log((item.subs === undefined))
            if(localStorage.getItem('user_type') === item.approval || item.approval === undefined){
                if(item.subs === undefined){
                    let color = ""
                    if(this.props.activeSubMenu === item.subMenuID){
                        ///*#007bff;*/ #3498db; /*#28a745;*/
                        color = '#3498db'
                    }
                    else{
                        color = ""
                    }
                    return (
                        <li id={'list__' + item.subMenuID} key={i} href={'#' + item.subMenuID} data-toggle="collapse"
                            aria-expanded="false" style={{cursor:"pointer"}}>
                                <div id={'DivMenu__' + item.subMenuID} className="sideBarMenuItem">
                                    <Link to={item.to} id={'Menu__' + item.subMenuID} className="sideBarMenuItem" style={{color:color}}>
                                        <i className={'fas ' + item.menuIcon}></i> {item.menuName}
                                    </Link>
                                </div>
                            <hr className="sideBarMenuDivider"/>
                        </li>
                    )
                }
                else{
                    return (
                        <li id={'list__' + item.subMenuID} key={i} data-target={'#' + item.subMenuID} href={'#' + item.subMenuID} data-toggle="collapse"
                            aria-expanded="false" onClick={this.toggleSubMenu} style={{cursor:"pointer"}}>
                            <div id={'DivMenu__' + item.subMenuID} className="sideBarMenuItem">
                                <a id={'Menu__' + item.subMenuID} data-target={'#' + item.subMenuID} href={'#' + item.subMenuID} data-toggle="collapse"
                                   aria-expanded="false"
                                   className="sideBarMenuItem">
                                    <i className={'fas ' + item.menuIcon}></i> {item.menuName}
                                    <i id={'button__' + item.subMenuID} className="fas fa-angle-left menuArrowIcon-left"></i>
                                </a>
                            </div>
                            <SideBarSubMenu id={item.subMenuID} subs={item.subs} activeSubMenu={this.props.activeSubMenu}/>
                            <hr className="sideBarMenuDivider"/>
                        </li>
                    )
                }
            }
        })

        return (
            <div className="sidebar_custom col-12" style={{padding: "0px 0px 0px 0px"}}>
                <nav>
                    <ul className="list-unstyled components">
                        {sideBarMenu}
                    </ul>
                </nav>
            </div>
        )
    }
}