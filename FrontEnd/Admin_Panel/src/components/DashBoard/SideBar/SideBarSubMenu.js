import React from 'react'
import {Link} from 'react-router-dom'
import './SideBar.css'

export default class SideBarSubMenu extends React.Component{
    render = () => {
        console.log(this.props.id)
        console.log()
        let subMenuItems = this.props.subs.map((item,i) => {
            let color = "";

            if(item.approval === undefined || item.approval === localStorage.getItem('user_type')){
                console.log(this.props.activeSubMenu + '===' + this.props.id + '_' + i)
            console.log(this.props.activeSubMenu === this.props.id + '_' + i)
            if(this.props.activeSubMenu === this.props.id + '_' + i){
                ///*#007bff;*/ #3498db; /*#28a745;*/
                color = '#3498db'
            }
            else{
                color = ""
            }

            return(
                <Link key={i} to={item.to}><li className="sideBarMenuItem">
                    <span id={this.props.id + '_' + i} className="sideBarSubMenuItem" style={{color:color}}><i className={'fas ' + item.icon}></i> {item.name}</span>
                </li></Link>
            )
            }
        })
        let listClass = ""

        console.log((this.props.id + '=== "list-unstyled collapse"'))
        console.log((this.props.id === "list-unstyled collapse"))
        if(this.props.id === localStorage.getItem('live_sub_menu')){
            listClass = "list-unstyled collapse show"
        }
        else{
            listClass = "list-unstyled collapse"
        }
        console.log(document.getElementById(this.props.id));
        return(
            <ul id={this.props.id} className={listClass}>
                {subMenuItems}
            </ul>
        );
    }
}