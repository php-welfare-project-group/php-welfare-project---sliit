import React from 'react'
import profile from '../../assets/img/40x40-walnut-main.jpg'
import {Link} from 'react-router-dom'

export default class Header extends React.Component {
    render = () => {
        return (
            <nav className="navbar sticky-top navbar-expand-lg navbar-dark bg-primary">
                <Link to="/" className="navbar-brand" href="#">Welfare</Link>
                <button className="navbar-toggler" type="button"
                        data-toggle="collapse" data-target="#navbarContent"
                        aria-label="Navbar Toggle Button"
                        aria-controls="navbarContent">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div id="navbarContent" className="collapse navbar-collapse">
                    <ul className="navbar-nav ml-auto">
                        <li className="nav-item">
                            <Link to="#" data-toggle="modal" data-target="#adminProfile" className="nav-link"><img className="rounded-circle" src={profile}
                                                                            width={30} height={30}
                                                                            alt="Profile"/></Link>
                        </li>
                        <li className="nav-item dropdown">
                            <Link to="/" className="nav-link dropdown-toggle" role="button"
                                  data-toggle="dropdown" data-target="#dropDownMenu"
                                  aria-haspopup="true" aria-expanded="false"
                                  id="dropDownButton" href="#">{localStorage.getItem('user_name')}</Link>
                            <div className="dropdown-menu dropdown-menu-right" aria-labelledby="dropDownButton">
                                <button className="dropdown-item" data-toggle="modal" data-target="#adminProfile">Account</button>
                                <button className="dropdown-item" onClick={this.props.setLoggedIn}>Log Out</button>
                                <div className="dropdown-divider"></div>
                                <button className="dropdown-item">Other</button>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        )
    }
}