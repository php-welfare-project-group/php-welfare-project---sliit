import React from 'react'
import axios from 'axios'
import restConfig from '../config/restConfig'
import EditEmployee from './EditEmployee'
import AssignEmployee from './AssignEmployee'
import SearchBar from '../SearchBar/SearchBar'
import ViewEmployee from './ViewEmployee'
import EmployeeFormConfig from '../config/formFields'
import permissions from '../config/permissions'

export default class EmployeeList extends React.Component {
    state = {
        employeeData: null,
        startPosition: 0,
        count: null
    }

    eraseEmployeeState = () => {
        this.setState({ employeeData: null })
    }

    changePosition = (e) => {
        this.setState({ employeeData: null })
        let startPosition = 0
        let identifier = e.target.id.split('_')

        console.log(identifier)
        if (identifier[1] === "next") {
            startPosition = this.state.startPosition + 5
        }
        else if (identifier[1] === "previous") {
            startPosition = this.state.startPosition - 5
        }
        else if (identifier[1] === "last") {
            if (this.state.count % 5 === 0) {
                startPosition = ((this.state.count / 5) - 1) * 5
            }
            else {
                startPosition = (Math.floor(this.state.count / 5) * 5)
            }
        }
        else if (identifier[1] === "first") {
            startPosition = 0
        }
        else if (parseInt(identifier[2]) > 0) {
            startPosition = 5 * (identifier[2] - 1)
        }
        this.setState({ startPosition: startPosition })
    }

    updateEmployeeStatus = (empId, status) => {
        return new Promise((resolve, reject) => {
            axios.put(restConfig.getRestAPIUrl('employees/' + empId), { "status": status }).then((response) => {
                console.log(response)
                if (response.status === 202) {
                    console.log('if is true')
                    resolve(true)
                }
                else {
                    resolve(false)
                }
            }).catch((error) => {
                reject(error)
            })
        })
    }

    activeOrDeactiveEmployee = (e) => {
        console.log(e.target.id)
        let htmlButton = e.target

        if (e.target.value === "Deactive" || e.target.value === "Approve") {
            this.updateEmployeeStatus(htmlButton.id, '1').then((response) => {
                if (response) {
                    console.log("deactive")
                    htmlButton.className = "btn btn-success"
                    htmlButton.value = "Active"
                }
            })
        }
        else {
            this.updateEmployeeStatus(htmlButton.id, '0').then((response) => {
                if (response) {
                    console.log("active")
                    htmlButton.className = "btn btn-danger"
                    htmlButton.value = "Deactive"
                }
            })
        }
    }

    deleteEmployee = (e) => {
        let empId = e.target.id

        if (window.confirm("Are you sure want to delete this Employee?")) {
            this.updateEmployeeStatus(empId, '-1').then((response) => {
                if (response) {
                    alert('Employee Deleted')
                }
                else {
                    alert('Employee Deletion failed')
                }

                this.setState({ employeeData: null })
            })
        }
    }

    getEmployeeData = (NIC) => {
        if (NIC === null || NIC === "") {
            //Get the relevant data and update the status of the component
            let headers = { "startPosition": this.state.startPosition }
            console.log(headers)
            axios.get(restConfig.getRestAPIUrl('employees'), { "headers": headers }).then((result) => {
                console.log(result)
                this.setState({ employeeData: result.data.data })
            }).catch((error) => {
                if (error.status === 204) {
                    this.setState({ employeeData: 'no data' })
                }
                else {
                    this.setState({ employeeData: 'error' })
                }
            })

            axios.get(restConfig.getRestAPIUrl('employees', ['count'])).then((result) => {
                console.log(result.data.count)
                this.setState({ count: result.data.count })
            }).catch((error) => {
                console.log(error)
            })
        }
        else {
            let options = ['nic', NIC]
            axios.get(restConfig.getRestAPIUrl('employees', options)).then((result) => {
                this.setState({ employeeData: result.data.data })
                this.setState({ count: result.data.rowCount })
                this.setState({ startPosition: 0 })
            }).catch((error) => {
                console.log(error)
                if (error.status === 204) {
                    this.setState({ employeeData: 'no data' })
                }
                else {
                    this.setState({ employeeData: 'error' })
                }
            })
        }
    }

    render = () => {
        let userPermissions = permissions.getPermissions(localStorage.getItem('user_type'))
        let editDataPermission = userPermissions.permissions.employees.update.data
        let editStatusPermission = userPermissions.permissions.employees.update.status
        let deletePermission = userPermissions.permissions.employees.delete
        let viewSinglePermission = userPermissions.permissions.employees.view.single
        let employeeAssignPermission = userPermissions.permissions.employees.assign
        
        let tableRows = null
        let nextLink = null
        let buttons = []
        let previousLink = null
        let firstButton = null
        let lastButton = null
        let editForms = []
        let viewForms = []
        let test1 = []
        let commonColumnWidth = "col-3"
        let statusColumnWidth = "col-4"
        let commonUpdateHeaders = []

        if (this.state.employeeData !== null) {
            if (this.state.employeeData === 'error') {
                tableRows = <tr><td colSpan={4} style={{ textAlign: "center" }}>An Error Occured While Connecting With Services. Sorry For Inconvienience</td></tr>
            }
            else {
                //Prepare table rows
                if (this.state.startPosition === 0) {
                    previousLink = null
                    firstButton = null
                }
                else {
                    firstButton = <button id="btn_first" className="btn btn-primary" onClick={this.changePosition}><i id="buttonSign_first" className="fas fa-angle-double-left"></i></button>
                    previousLink = <button id="btn_previous" className="btn btn-primary" onClick={this.changePosition}><i id="buttonSign_previous" className="fas fa-angle-left"></i></button>
                }

                let rowCount = this.state.count
                let i = 1
                while (rowCount > 0) {
                    if (this.state.startPosition !== (5 * (i - 1))) {
                        buttons.push(<button key={i} className="btn btn-primary" id={"btn_page_" + i} onClick={this.changePosition}>{i}</button>)
                    }
                    else {
                        buttons.push(<button key={i} className="btn btn-secondary" id={"btn_page_" + i} onClick={this.changePosition}>{i}</button>)
                    }
                    rowCount -= 5
                    i++;
                }

                if (this.state.startPosition < (this.state.count - 5)) {
                    lastButton = <button id="btn_last" className="btn btn-primary" onClick={this.changePosition}><i id="buttonSign_last" className="fas fa-angle-double-right"></i></button>
                    nextLink = <button id="btn_next" className="btn btn-primary" onClick={this.changePosition}><i id="buttonSign_next" className="fas fa-angle-right"></i></button>
                }
                else {
                    lastButton = null
                    nextLink = null
                }

                if (this.state.employeeData !== undefined || this.state.employeeData === "no data") {
                    tableRows = this.state.employeeData.map((item, i) => {
                        // let statusColumn = ""
                        let status = ""
                        if(editStatusPermission){
                            if (item.status === '0') {
                                status = <input type="button" id={item.empId} className="btn btn-danger" value="Deactive" onClick={this.activeOrDeactiveEmployee} />
                            }
                            else if(item.status === '-2'){
                                status = <input type="button" id={item.empId} className="btn btn-default" value="Approve" onClick={this.activeOrDeactiveEmployee} />
                            }
                            else {
                                status = <input type="button" id={item.empId} className="btn btn-success" value="Active" onClick={this.activeOrDeactiveEmployee} />
                            }
                        }
                        else{
                            if(item.status === '0'){
                                status = <label style={{color:"red"}}>Deactive</label>
                            }
                            else if(item.status === '-2'){
                                status = <label>Need Approval</label>
                            }
                            else{
                                status = <label style={{color:"green"}}>Active</label>
                            }
                            // commonColumnWidth = "col-4"
                        }
                        let statusColumn = <td className="col-2" style={{ textAlign: "center" }}>{status}</td>

                        let data = {
                            empId: item.empId,
                            nic: item.nic,
                            fName: item.fName,
                            mName: item.mName,
                            lName: item.lName,
                            dob: item.dob,
                            address: item.address,
                            district: item.district,
                            divisionSecretariat: item.divisionSecretariat,
                            gramaNilaDivision: item.gramaNilaDivision,
                            designation: item.designation,
                            officialName: item.officialName,
                            dateOfAppointment: item.dateOfAppointment,
                            dateOfRetirement: item.dateOfRetirement,
                            whatsappNo: item.whatsappNo,
                            imoNo: item.imoNo,
                            mobileNo: item.mobileNo,
                            homePhone: item.homePhone,
                            eduQualification: item.eduQualification,
                            profQualification: item.profQualification,
                            status: item.status,
                            role: item.role
                        }

                        let dataObject = EmployeeFormConfig.getEmployeeFormFields(data)
                        let editButton = ""
                        let viewSingleButton = ""
                        let deleteButton = ""
                        let assignButton = ""

                        if(editDataPermission || viewSinglePermission || employeeAssignPermission || deletePermission){
                            let statusWidthCount = 1
                            if(editDataPermission){
                                editForms.push(<EditEmployee key={i} editData={dataObject} eraseEmployeeState={this.eraseEmployeeState} />)
                                editButton = <div style={{ display: "inline-flex", paddingRight: "10px" }}>
                                <input type="button" id={item.empId} name="Edit" className="btn btn-primary" value="Edit" data-toggle="modal"
                                    data-target={"#editEmployee_" + item.empId} />
                            </div>
                            statusWidthCount++
                            }
                            else{
                                // statusColumnWidth = "c"
                            }
    
                            if(viewSinglePermission){
                                viewForms.push(<ViewEmployee key={i} viewData={dataObject} />)
                                viewSingleButton = <div style={{ display: "inline-flex", paddingRight: "10px" }}>
                                <input type="button" id={item.empId} name="View" className="btn btn-success" value="View" data-toggle="modal"
                                    data-target={"#viewEmployee_" + item.empId} />
                            </div>
                            statusWidthCount++
                            }
    
                            if(employeeAssignPermission){
                                test1.push(<AssignEmployee key={i} empId={item.empId} nic={item.nic} fName={item.fName} mName={item.mName}
                                    lName={item.lName} />)
                                    assignButton = <div style={{ display: "inline-flex", paddingRight: "10px" }}>
                                    <input type="button" id={item.empId} name="Assign" className="btn btn-primary" value="Assign" data-toggle="modal"
                                        data-target={"#assignEmployee_" + item.empId} />
                                </div>
                                statusWidthCount++
                            }

                            if(deletePermission){
                                deleteButton = <div style={{ display: "inline-flex" }}>
                                                    <input type="button" id={item.empId} className="btn btn-danger" value="Delete" onClick={this.deleteEmployee} />
                                                </div>
                                statusWidthCount++
                            }

                            (statusWidthCount != 1) ? statusColumnWidth -= 2 : statusColumnWidth = 1
                            statusColumnWidth = "col-" + statusWidthCount
                        }
                        return (
                            <tr key={i}>
                                <td className={commonColumnWidth}>{item.nic}</td>
                                <td className={commonColumnWidth}>{item.fName} {item.lName}</td>
                                {statusColumn}
                                <td className={statusColumnWidth} style={{ textAlign: "center" }}>
                                    {viewSingleButton}
                                    {assignButton}
                                    {editButton}
                                    {deleteButton}
                                </td>
                            </tr>
                        )
                    })
                }
                else {
                    tableRows = <tr><td colSpan={4} style={{ textAlign: "center" }}>No employees available in the database or you have deleted all the employees related to this page.</td></tr>
                }
            }
        }
        else {
            tableRows = <tr><td colSpan={4} style={{ textAlign: "center" }}>Please wait</td></tr>
            this.getEmployeeData(null);
        }

        // if(editStatusPermission){
            // commonUpdateHeaders.push(<th className="col-2" style={{ textAlign: "center" }}>Status</th>)
        // }

        if(editDataPermission || deletePermission || employeeAssignPermission || viewSinglePermission){
            commonUpdateHeaders.push(<th className={statusColumnWidth} style={{ textAlign: "center" }}>Operation</th>)
        }

        // let tableWidth = ""
        // (localStorage.getItem('user_type') === "Site Admin") ? tableWidth = "col-md-" + (parseInt(commonColumnWidth.split('-')[0]) + parseInt(statusColumnWidth.split('-')[0])) : tableWidth = "col-md-12"
        return (
            <div className="col-md-12" style={{ padding: "10px" }}>
                <h4>Employee List</h4>
                <hr />
                <SearchBar searchFunction={this.getEmployeeData} />
                <div className = "col-md-12" style={{paddingBottom:"inherit",margin:"auto"}}>
                    <table className="col-md-12 table table-striped table-bordered table-hover" style={{margin:"auto",maxWidth:"none"}}>
                        <thead>
                            <tr>
                                <th className={commonColumnWidth} style={{ textAlign: "center" }}>NIC</th>
                                <th className={commonColumnWidth} style={{ textAlign: "center" }}>Employee Name</th>
                                <th className="col-2" style={{ textAlign: "center" }}>Status</th>
                                {commonUpdateHeaders}
                            </tr>
                        </thead>
                        <tbody>
                            {tableRows}
                        </tbody>
                    </table>
                </div>
                <div style={{ float: "right" }}>
                    <div className="btn-group" role="group" aria-label="Basic example" style={{ marginLeft: "10px", marginRight: "10px" }}>
                        {firstButton}
                        {previousLink}
                        {buttons}
                        {nextLink}
                        {lastButton}
                    </div>
                </div>
                {editForms}
                {viewForms}
                {test1}
            </div>
        )
    }
}
