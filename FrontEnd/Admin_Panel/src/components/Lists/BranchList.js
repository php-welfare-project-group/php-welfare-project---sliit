import React from 'react'
import axios from 'axios'
import restConfig from '../config/restConfig'
import EditBranch from "./EditBranch"

export default class BranchList extends React.Component {
    state = {
        branchData: null,
        startPosition: 0,
        count: null
    }

    eraseBranchDataState = () => {
        this.setState({ branchData: null })
    }

    changePosition = (e) => {
        this.setState({ branchData: null })
        let startPosition = 0
        let identifier = e.target.id.split('_')

        console.log(identifier)
        if (identifier[1] === "next") {
            startPosition = this.state.startPosition + 5
        }
        else if (identifier[1] === "previous") {
            startPosition = this.state.startPosition - 5
        }
        else if (identifier[1] === "last") {
            if (this.state.count % 5 === 0) {
                startPosition = ((this.state.count / 5) - 1) * 5
            }
            else {
                startPosition = (Math.floor(this.state.count / 5) * 5)
            }
        }
        else if (identifier[1] === "first") {
            startPosition = 0
        }
        else if (parseInt(identifier[2]) > 0) {
            startPosition = 5 * (identifier[2] - 1)
        }
        console.log(startPosition)
        this.setState({ startPosition: startPosition })
    }

    checkSections = (branchId) => {
        let headers = { 'branchId': branchId }
        return new Promise((resolve, reject) => {
            axios.get(restConfig.getRestAPIUrl('section'), { "headers": headers }).then((response) => {
                console.log(response)
                if (response.status === 204) {
                    console.log('if is true')
                    resolve(0)
                }
                else {
                    resolve(response.data.data.length)
                }
            }).catch((error) => {
                reject(error)
            })
        })
    }

    updateBranchStatus = (branchId, status) => {
        return new Promise((resolve, reject) => {
            axios.put(restConfig.getRestAPIUrl('branch/' + branchId), { "status": status }).then((response) => {
                console.log(response)
                if (response.status === 202) {
                    console.log('if is true')
                    resolve(true)
                }
                else {
                    resolve(false)
                }
            }).catch((error) => {
                reject(error)
            })
        })
    }

    activeOrDeactiveBranch = (e) => {
        console.log(e.target.id)
        let htmlButton = e.target

        if (e.target.value === "Deactive") {
            this.updateBranchStatus(htmlButton.id, '1').then((response) => {
                if (response) {
                    console.log("deactive")
                    htmlButton.className = "btn btn-success"
                    htmlButton.value = "Active"
                }
            })
        }
        else {
            this.checkSections(e.target.id).then((response) => {
                console.log('section response : ' + response)
                if (response === 0) {
                    if (htmlButton.value === "Active") {
                        this.updateBranchStatus(htmlButton.id, '0').then((response) => {
                            if (response) {
                                console.log("active")
                                htmlButton.className = "btn btn-danger"
                                htmlButton.value = "Deactive"
                            }
                        })
                    }
                }
                else {
                    if (response > 1) {
                        alert('Can\'t deactivate branch. There are ' + response + ' active sections under selected branch')
                    }
                    else {
                        alert('Can\'t deactivate branch. There is ' + response + ' active section under selected branch')
                    }
                }
            })
        }
    }

    deleteBranch = (e) => {
        let branchId = e.target.id
        if (window.confirm("Are you sure want to delete this Branch?")) {
            this.checkSections(e.target.id).then((response) => {
                if (response === 0) {
                    this.updateBranchStatus(branchId, '-1').then((response) => {
                        if (response) {
                            alert('Branch Deleted')
                        }
                        else {
                            alert('Branch Deletion failed')
                        }

                        this.setState({ branchData: null })
                    })
                } else {
                    if (response > 1) {
                        alert('Can\'t deactivate branch. There are ' + response + ' active sections under selected branch')
                    }
                    else {
                        alert('Can\'t deactivate section. There is ' + response + ' active section under selected branch')
                    }
                }
            })
        }
    }

    render = () => {
        let tableRows = null
        let nextLink = null
        let buttons = []
        let previousLink = null
        let firstButton = null
        let lastButton = null
        let editForms = []
        if (this.state.branchData !== null) {
            if (this.state.branchData === 'error') {
                tableRows = <tr><td colSpan={4} style={{ textAlign: "center" }}>An Error Occured While Connecting With Services. Sorry For Inconvienience</td></tr>
            }
            else {
                //Prepare table rows
                console.log(this.state.branchData)

                if (this.state.startPosition === 0) {
                    previousLink = null
                    firstButton = null
                }
                else {
                    firstButton = <button id="btn_first" className="btn btn-primary" onClick={this.changePosition}><i id="buttonSign_first" className="fas fa-angle-double-left"></i></button>
                    previousLink = <button id="btn_previous" className="btn btn-primary" onClick={this.changePosition}><i id="buttonSign_previous" className="fas fa-angle-left"></i></button>
                }

                let rowCount = this.state.count
                let i = 1
                while (rowCount > 0) {
                    if (this.state.startPosition !== (5 * (i - 1))) {
                        buttons.push(<button key={i} className="btn btn-primary" id={"btn_page_" + i} onClick={this.changePosition}>{i}</button>)
                    }
                    else {
                        buttons.push(<button key={i} className="btn btn-secondary" id={"btn_page_" + i} onClick={this.changePosition}>{i}</button>)
                    }
                    rowCount -= 5
                    i++;
                }

                if (this.state.startPosition < (this.state.count - 5)) {
                    lastButton = <button id="btn_last" className="btn btn-primary" onClick={this.changePosition}><i id="buttonSign_last" className="fas fa-angle-double-right"></i></button>
                    nextLink = <button id="btn_next" className="btn btn-primary" onClick={this.changePosition}><i id="buttonSign_next" className="fas fa-angle-right"></i></button>
                }
                else {
                    //firstButton = null
                    lastButton = null
                    nextLink = null
                }

                if (this.state.branchData !== undefined  || this.state.branchData === "no data") {
                    tableRows = this.state.branchData.map((item, i) => {
                        let status = ""
                        if (item.status === '0') {
                            status = <input type="button" id={item.branchId} className="btn btn-danger" value="Deactive" onClick={this.activeOrDeactiveBranch} />
                        }
                        else {
                            status = <input type="button" id={item.branchId} className="btn btn-success" value="Active" onClick={this.activeOrDeactiveBranch} />
                        }

                        editForms.push(<EditBranch key={i} branchId={item.branchId} name={item.name} deptID={item.deptID} deptName={item.deptName} eraseBranchState={this.eraseBranchDataState} />)

                        return (
                            <tr className="d-flex" key={i}>
                                <td className="col-4">{item.deptName}</td>
                                <td className="col-4">{item.name}</td>
                                <td className="col-2" style={{ textAlign: "center" }}>{status}</td>
                                <td className="col-2" style={{ textAlign: "center" }}>
                                    <div style={{ display: "inline-flex", paddingRight: "10px" }}>
                                        <input type="button" id={item.branchId} className="btn btn-primary" value="Edit" data-toggle="modal"
                                            data-target={"#editBranch_" + item.branchId} />
                                    </div>
                                    <div style={{ display: "inline-flex" }}>
                                        <input type="button" id={item.branchId} className="btn btn-danger" value="Delete" onClick={this.deleteBranch} />
                                    </div>
                                </td>
                            </tr>
                        )
                    })
                }
                else {
                    tableRows = <tr className="d-flex"><td className="col-12" style={{ textAlign: "center" }}>No branches available in the database or you have deleted all the branches related to this page.</td></tr>
                }
            }
        }
        else {
            tableRows = <tr><td colSpan={4} style={{ textAlign: "center" }}>Please wait</td></tr>
            //Get the relevant data and update the status of the component
            let headers = { "startPosition": this.state.startPosition }
            // console.log(headers)
            axios.get(restConfig.getRestAPIUrl('branch'), { "headers": headers }).then((result) => {
                console.log(result.data)
                this.setState({ branchData: result.data.data })
            }).catch((error) => {
                if(error.status === 204){
                    this.setState({ branchData: 'no data' })
                }
                else{
                    this.setState({ branchData: 'error' })
                }
            })

            axios.get(restConfig.getRestAPIUrl('branch', ['count'])).then((result) => {
                // console.log(result.data.count)
                this.setState({ count: result.data.count })
            }).catch((error) => {
                console.log(error)
            })
            console.log(this.state.branchData)
        }
        return (
            <div className="col-md-12" style={{ padding: "10px" }}>
                <h4>Branch List</h4>
                <hr />
                <table className="table table-striped table-bordered table-hover">
                    <thead>
                        <tr className="d-flex">
                            <th className="col-4" style={{ textAlign: "center" }}>Department</th>
                            <th className="col-4" style={{ textAlign: "center" }}>Branch Name</th>
                            <th className="col-2" style={{ textAlign: "center" }}>Status</th>
                            <th className="col-2" style={{ textAlign: "center" }}>Operation</th>
                        </tr>
                    </thead>
                    <tbody>
                        {tableRows}
                    </tbody>
                </table>
                <div style={{ float: "right" }}>
                    <div className="btn-group" role="group" aria-label="Basic example" style={{ marginLeft: "10px", marginRight: "10px" }}>
                        {firstButton}
                        {previousLink}
                        {buttons}
                        {nextLink}
                        {lastButton}
                    </div>
                </div>
                {editForms}
            </div>
        )
    }
}
