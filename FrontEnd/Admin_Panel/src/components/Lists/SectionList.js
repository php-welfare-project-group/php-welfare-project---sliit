import React from 'react'
import axios from 'axios'
import restConfig from '../config/restConfig'
import EditSection from "./EditSection"
import permissions from '../config/permissions'

export default class SectionList extends React.Component {
    state = {
        sectionData: null,
        startPosition: 0,
        count: null,
    }

    eraseSectionDataState = () => {
        this.setState({ sectionData: null })
    }

    changePosition = (e) => {
        this.setState({ sectionData: null })
        let startPosition = 0
        let identifier = e.target.id.split('_')

        console.log(identifier)
        if (identifier[1] === "next") {
            startPosition = this.state.startPosition + 5
        }
        else if (identifier[1] === "previous") {
            startPosition = this.state.startPosition - 5
        }
        else if (identifier[1] === "last") {
            if (this.state.count % 5 === 0) {
                startPosition = ((this.state.count / 5) - 1) * 5
            }
            else {
                startPosition = (Math.floor(this.state.count / 5) * 5)
            }
        }
        else if (identifier[1] === "first") {
            startPosition = 0
        }
        else if (parseInt(identifier[2]) > 0) {
            startPosition = 5 * (identifier[2] - 1)
        }
        this.setState({ startPosition: startPosition })
    }

    checkEmployees = (sectionId) => {
        let headers = { 'sectionId': sectionId }
        return new Promise((resolve, reject) => {
            axios.get(restConfig.getRestAPIUrl('employees'), { "headers": headers }).then((response) => {
                console.log(response)
                if (response.status === 204) {
                    console.log('if is true')
                    resolve(0)
                }
                else {
                    resolve(response.data.data.length)
                }
            }).catch((error) => {
                reject(error)
            })
        })
    }

    updateSectionStatus = (sectionId, status) => {
        return new Promise((resolve, reject) => {
            axios.put(restConfig.getRestAPIUrl('section/' + sectionId), { "status": status }).then((response) => {
                console.log(response)
                if (response.status === 202) {
                    console.log('if is true')
                    resolve(true)
                }
                else {
                    resolve(false)
                }
            }).catch((error) => {
                reject(error)
            })
        })
    }

    activeOrDeactiveSection = (e) => {
        console.log(e.target.id)
        let htmlButton = e.target

        if (e.target.value === "Deactive" || e.target.value === "Approve") {
            this.updateSectionStatus(htmlButton.id, '1').then((response) => {
                if (response) {
                    console.log("deactive")
                    htmlButton.className = "btn btn-success"
                    htmlButton.value = "Active"
                }
            })
        }
        else {
            this.checkEmployees(e.target.id).then((response) => {
                console.log('section response : ' + response)
                if (response === 0) {
                    if (htmlButton.value === "Active") {
                        this.updateSectionStatus(htmlButton.id, '0').then((response) => {
                            if (response) {
                                console.log("active")
                                htmlButton.className = "btn btn-danger"
                                htmlButton.value = "Deactive"
                            }
                        })
                    }
                }
                else {
                    if (response > 1) {
                        alert('Can\'t deactivate section. There are ' + response + ' active employees under selected section')
                    }
                    else {
                        alert('Can\'t deactivate section. There is ' + response + ' active employee under selected section')
                    }
                }
            })
        }
    }

    deleteSection = (e) => {
        let sectionId = e.target.id

        if (window.confirm("Are you sure want to delete this Section?")) {
            this.checkEmployees(sectionId).then((r) => {
                if (r === 0) {
                    this.updateSectionStatus(sectionId, '-1').then((response) => {
                        if (response) {
                            alert('Section Deleted')
                        }
                        else {
                            alert('Section Deletion failed')
                        }

                        this.setState({ sectionData: null })
                    })

                } else {
                    if (r > 1) {
                        alert('Can\'t deactivate section. There are ' + r + ' active employees under selected section')
                    }
                    else {
                        alert('Can\'t deactivate section. There is ' + r + ' active employee under selected section')
                    }
                }

            })
        }
    }

    render = () => {
        let userPermissions = permissions.getPermissions(localStorage.getItem('user_type'))
        let editDataPermission = userPermissions.permissions.sections.update.data
        let editStatusPermission = userPermissions.permissions.sections.update.status
        let deletePermission = userPermissions.permissions.sections.delete
        console.log(userPermissions)

        let tableRows = null
        let nextLink = null
        let buttons = []
        let previousLink = null
        let firstButton = null
        let lastButton = null
        let editForms = []
        let commonColumnWidth = "col-md-4"
        let commonUpdateHeaders = []
        if (this.state.sectionData !== null) {
            if (this.state.sectionData === 'error') {
                tableRows = <tr><td colSpan={4} style={{ textAlign: "center" }}>An Error Occured While Connecting With Services. Sorry For Inconvienience</td></tr>
            }
            else {
                //Prepare table rows
                console.log(this.state.sectionData)

                if (this.state.startPosition === 0) {
                    previousLink = null
                    firstButton = null
                }
                else {
                    firstButton = <button id="btn_first" className="btn btn-primary" onClick={this.changePosition}><i id="buttonSign_first" className="fas fa-angle-double-left"></i></button>
                    previousLink = <button id="btn_previous" className="btn btn-primary" onClick={this.changePosition}><i id="buttonSign_previous" className="fas fa-angle-left"></i></button>
                }

                let rowCount = this.state.count
                let i = 1
                while (rowCount > 0) {
                    if (this.state.startPosition !== (5 * (i - 1))) {
                        buttons.push(<button key={i} className="btn btn-primary" id={"btn_page_" + i} onClick={this.changePosition}>{i}</button>)
                    }
                    else {
                        buttons.push(<button key={i} className="btn btn-secondary" id={"btn_page_" + i} onClick={this.changePosition}>{i}</button>)
                    }
                    rowCount -= 5
                    i++;
                }

                if (this.state.startPosition < (this.state.count - 5)) {
                    lastButton = <button id="btn_last" className="btn btn-primary" onClick={this.changePosition}><i id="buttonSign_last" className="fas fa-angle-double-right"></i></button>
                    nextLink = <button id="btn_next" className="btn btn-primary" onClick={this.changePosition}><i id="buttonSign_next" className="fas fa-angle-right"></i></button>
                }
                else {
                    //firstButton = null
                    lastButton = null
                    nextLink = null
                }

                if (this.state.sectionData !== undefined || this.state.sectionData === "no data") {
                    tableRows = this.state.sectionData.map((item, i) => {
                        let status = ""
                        
                        if(editStatusPermission){
                            if (item.status === '0') {
                                status = <input type="button" id={item.sectionId} className="btn btn-danger" value="Deactive" onClick={this.activeOrDeactiveSection} />
                            }
                            else if(item.status === '-2'){
                                status = <input type="button" id={item.sectionId} className="btn btn-default" value="Approve" onClick={this.activeOrDeactiveSection} />
                            }
                            else {
                                status = <input type="button" id={item.sectionId} className="btn btn-success" value="Active" onClick={this.activeOrDeactiveSection} />
                            }
                        }
                        else{
                            if(item.status === '0'){
                                status = <label style={{color:"red"}}>Deactive</label>
                            }
                            else if(item.status === '-2'){
                                status = <label>Need Approval</label>
                            }
                            else{
                                status = <label style={{color:"green"}}>Active</label>
                            }
                        }

                        let editDataButton = null
                        let deleteButton = null
                        if(editDataPermission || deletePermission){
                            if(editDataPermission){
                                editDataButton = <div style={{ display: "inline-flex", paddingRight: "10px" }}>
                                                    <input type="button" id={item.sectionId} className="btn btn-primary" value="Edit" data-toggle="modal"
                                                        data-target={"#editSection_" + item.sectionId} />
                                                 </div>
                                editForms.push(<EditSection key={i} sectionId={item.sectionId} sectionName={item.sectionName} branchId={item.branchId} name={item.name} eraseSectionState={this.eraseSectionDataState} />)                 
                            }
                            if(deletePermission){
                                deleteButton = <div style={{ display: "inline-flex" }}>
                                                    <input type="button" id={item.sectionId} className="btn btn-danger" value="Delete" onClick={this.deleteSection} />
                                                </div>
                            }
                        }

                        return (
                            <tr className="d-flex" key={i}>
                                <td className={commonColumnWidth}>{item.sectionName}</td>
                                <td className={commonColumnWidth}>{item.name}</td>
                                <td className="col-2" style={{ textAlign: "center" }}>{status}</td>
                                <td className="col-2" style={{ textAlign: "center" }}>
                                {editDataButton}
                                {deleteButton}
                                </td>
                            </tr>
                        )
                    })
                }
                else {
                    tableRows = <tr className="d-flex"><td className="col-12" style={{ textAlign: "center" }}>No sections available in the database or you have deleted all the sections related to this page.</td></tr>
                }
            }
        }
        else {
            tableRows = <tr><td colSpan={4} style={{ textAlign: "center" }}>Please wait</td></tr>
            //Get the relevant data and update the status of the component
            let headers = { "startPosition": this.state.startPosition }
            console.log(headers)
            axios.get(restConfig.getRestAPIUrl('section'), { "headers": headers }).then((result) => {
                console.log(result)
                this.setState({ sectionData: result.data.data })
            }).catch((error) => {
                if(error.status === 204){
                    this.setState({ sectionData: 'no data' })
                }
                else{
                    this.setState({ sectionData: 'error' })
                }
            })

            axios.get(restConfig.getRestAPIUrl('section', ['count'])).then((result) => {
                console.log(result.data.count)
                this.setState({ count: result.data.count })
            })
            // .catch((error) => {
            //     this.setState({ sectionData: 'error' })
            // })
        }

        // if(editStatusPermission){
        //     commonUpdateHeaders.push(<th className="col-2" style={{ textAlign: "center" }}>Status</th>)
        // }

        if(editDataPermission){
            commonUpdateHeaders.push(<th className="col-2" style={{ textAlign: "center" }}>Operations</th>)
        }
        return (
            <div className="col-md-12" style={{ padding: "10px" }}>
                <h4>Branch List</h4>
                <hr />
                <table className="table table-striped table-bordered table-hover">
                    <thead>
                        <tr className="d-flex">
                            <th className={commonColumnWidth} style={{ textAlign: "center" }}>Section Name</th>
                            <th className={commonColumnWidth} style={{ textAlign: "center" }}>Branch Name</th>
                            <th className="col-2" style={{ textAlign: "center" }}>Status</th>
                            {commonUpdateHeaders}
                        </tr>
                    </thead>
                    <tbody>
                        {tableRows}
                    </tbody>
                </table>
                <div style={{ float: "right" }}>
                    <div className="btn-group" role="group" aria-label="Basic example" style={{ marginLeft: "10px", marginRight: "10px" }}>
                        {firstButton}
                        {previousLink}
                        {buttons}
                        {nextLink}
                        {lastButton}
                    </div>
                </div>
                {editForms}
            </div>
        )
    }
}
