import React from 'react'
import axios from 'axios'
import restConfig from '../config/restConfig'

export default class EditSection extends React.Component{
    state = {
      branchList:null
    }

    saveInputs = (e) => {
        this.setState({[e.target.name]:e.target.value})
    }

    submitForm = () => {
      let sectionName = document.getElementById('txtSectionName_' + this.props.sectionId).value
      let branchId = document.getElementById('chkBranchName_'  + this.props.sectionId).value

      let transferObject = {
          sectionName:sectionName,
          branchId:branchId,
          status:'-2'
      }
      console.log(this.props.sectionId)
      console.log(transferObject)
      if((sectionName !== this.props.sectionName) | (branchId !== this.props.branchId)){
          axios.put(restConfig.getRestAPIUrl('section/' + this.props.sectionId),transferObject).then((response)=> {
              if(response.status === 202){
                  alert('Section Updated')
                  this.props.eraseSectionState()
              }else{
                  alert('An error has occurred')
              }
          })
      }
    }

    setDefaultValues = () => {
        document.getElementById('txtSectionName_' + this.props.sectionId).value = this.props.sectionName
        // document.getElementById('chkBranchName_').value = this.props.branchId
    }

    render = () => {
      let branchList = null
      let headers = {'active':true}
      if(this.state.branchList === null){
        axios.get(restConfig.getRestAPIUrl('branch'),{"headers":headers}).then((response) => {
            this.setState({branchList:response.data.data})
        })
      }
      else{
          branchList =  this.state.branchList.map((item,i) => {
            if (item.branchId === this.props.branchId){
              return <option key={i} value={item.branchId} selected>{item.name}</option>
            } else {
              return <option key={i} value={item.branchId}>{item.name}</option>
            }
          })
      }
        return (
          <div className="modal fade" id={"editSection_" + this.props.sectionId} role="dialog"
                 aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content col-md-12">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLongTitle">Edit Section</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                        </div>
                        <div className="modal-body col-md-12">
                          <form>
                              <div className="form-group row">
                                  <label htmlFor={"txtSectionName_" + this.props.sectionId} className="col-md-2 col-form-label">Section Name</label>
                                  <div className="col-md-10" style={{marginTop:"12px"}}>
                                      <input type="text" className="form-control" id={"txtSectionName_" + this.props.sectionId} name="sectionName" onChange={this.saveInputs} defaultValue={this.props.sectionName}/>
                                  </div>
                              </div>
                              <div className="form-group row">
                                  <label htmlFor={"chkBranchName_" + this.props.sectionId} className="col-md-2 col-form-label">Branch Name</label>
                                  <div className="col-md-10" style={{marginTop:"12px"}}>
                                      <select id={"chkBranchName_" + this.props.sectionId} name="branchId" className="form-control"
                                       onChange={this.saveInputs} style={{overFlow:"scroll"}}>
                                          {branchList}
                                      </select>
                                  </div>
                              </div>
                          </form>
                          <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-dismiss="modal" onClick={this.setDefaultValues}>Close</button>
                            <button type="submit" className="btn btn-primary" data-dismiss="modal" onClick={this.submitForm} >Save changes</button>
                          </div>
                      </div>
                   </div>
              </div>
          </div>
        )

    }
  }
