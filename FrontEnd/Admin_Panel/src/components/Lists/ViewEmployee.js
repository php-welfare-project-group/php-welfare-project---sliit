import React from 'react'

export default class ViewEmployee extends React.Component {
    render = () => {
        let showFields = []
        let employeeViewObject = this.props.viewData

        for (var mainKeys in employeeViewObject) {
            let mainAttributeObject = employeeViewObject[mainKeys]
            let dataPart = mainAttributeObject.data
            let delimeter = null
            let value = ""

            if (dataPart !== null) {
                if (typeof dataPart === "object") {
                    delimeter = dataPart.delimeter
                    let counter = 1
                    for (var dataKey in dataPart) {
                        if (dataKey !== 'delimeter' && dataPart[dataKey] !== null) {
                            if (value === "") {
                                value += dataPart[dataKey]
                            }
                            else {
                                value += delimeter + dataPart[dataKey]
                            }
                        }

                        if (mainKeys === "contactNo") {
                            switch (counter) {
                                case 1: if(dataPart[dataKey] !== null) {value += " (Whatsapp)"}; break;
                                case 2: if(dataPart[dataKey] !== null) { value += " (Imo)"}; break;
                                case 3: if(dataPart[dataKey] !== null) { value += " (Mobile)"}; break;
                                default: value += "";break;
                            }
                            counter++
                        }
                    }
                }
                else {
                    if(mainKeys === "status"){
                        if(dataPart === '1'){
                            value += "Active"
                        }
                        else{
                            value += "Deactive"
                        }
                    }
                    else{
                        value += dataPart
                    }
                }

                showFields.push(<div className="form-group row">
                    <label className="col-md-4 col-form-label"><strong>{mainAttributeObject.caption}</strong></label>
                    <div className="col-md-7">
                        <label className="col-form-label">{value}</label>
                    </div>
                </div>)
            }
        }

        return (
            <div className="modal fade" id={"viewEmployee_" + this.props.viewData.empId.data} role="dialog"
                aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                <div className="modal-dialog modal-md" role="document">
                    <div className="modal-content col-md-12">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLongTitle">{this.props.viewData.name.data.fName + " " +
                                this.props.viewData.name.data.mName + " " + this.props.viewData.name.data.lName + " Employee Details"}</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body col-md-12">
                            <form id="employeeViewForm">
                                {showFields}
                            </form>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-secondary" data-dismiss="modal" onClick={this.setDefaultValues}>Close</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}