import React from 'react'
import axios from 'axios'
import restConfig from '../config/restConfig'

export default class AssignEmployee extends React.Component{
    state = {
      sectionList:null
    }

    saveInputs = (e) => {
        this.setState({[e.target.name]:e.target.value})
    }

    submitForm = () => {
      let empId = this.props.empId
      let sectionId = this.state.sectionId
      let startDate = this.state.startDate
      let endDate = this.state.endDate

      let transferObject = {
          empId:empId,
          sectionId:sectionId,
          startDate:startDate,
          endDate:endDate
      }

      console.log(transferObject)
      axios.post(restConfig.getRestAPIUrl('worksin'),transferObject).then((response)=> {
          if(response.status === 203){
              alert('Employee Assigned')
          }else{
              alert('An error has occurred')
          }
      })

    }

    render = () => {
      let sectionList = null
      let headers = {"active":true}
      if(this.state.sectionList === null){
          axios.get(restConfig.getRestAPIUrl('section'),{headers:headers}).then((response) => {
              this.setState({sectionList:response.data.data})
          })
      }
      else{
          sectionList = this.state.sectionList.map((item, i) => {
              return <option key={i} value={item.sectionId}>{item.sectionName}</option>
          })
      }
        return (
          <div className="modal fade" id={"assignEmployee_" + this.props.empId} role="dialog"
                 aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content col-md-12">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLongTitle">Assign Employee</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                        </div>
                        <div className="modal-body col-md-12">
                          <form>
                              <div className="form-group row">
                                  <label htmlFor={"txtNic_" + this.props.empId} className="col-md-2 col-form-label">Nic Number</label>
                                  <div className="col-md-10" style={{marginTop:"12px"}}>
                                      <input type="text" className="form-control" id={"txtNic_" + this.props.empId} name="nic" placeholder={this.props.nic} readOnly/>
                                  </div>
                              </div>
                              <div className="form-group row">
                                  <label htmlFor={"txtSection_" + this.props.empId} className="col-md-2 col-form-label">Assign To</label>
                                  <div className="col-md-10" style={{marginTop:"12px"}}>
                                      <select id={"txtSection_" + this.props.empId} name="sectionId" className="form-control"
                                       onChange={this.saveInputs} style={{overFlow:"scroll"}} defaultValue={0}>
                                          <option value={0} disabled>Choose here</option>
                                          {sectionList}
                                      </select>
                                  </div>
                              </div>
                              <div className="form-group row">
                                  <label htmlFor={"txtStartDate_" + this.props.empId} className="col-md-2 col-form-label">Start Date</label>
                                    <div className="col-md-10" style={{marginTop:"12px"}}>
                                        <input type="date" className="form-control" id={"txtStartDate_" + this.props.empId} name="startDate" onChange={this.saveInputs}/>
                                    </div>
                              </div>
                              <div className="form-group row">
                                  <label htmlFor={"txtEndDate_" + this.props.empId} className="col-md-2 col-form-label">End Date</label>
                                    <div className="col-md-10" style={{marginTop:"12px"}}>
                                        <input type="date" className="form-control" id={"txtEndDate_" + this.props.empId} name="endDate" onChange={this.saveInputs}/>
                                        <small className="form-text text-muted">Optional</small>
                                    </div>
                              </div>
                          </form>
                          <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-dismiss="modal" onClick={this.setDefaultValues}>Close</button>
                            <button type="submit" className="btn btn-primary" data-dismiss="modal" onClick={this.submitForm} >Save changes</button>
                          </div>
                      </div>
                   </div>
              </div>
          </div>
        )

    }
  }
