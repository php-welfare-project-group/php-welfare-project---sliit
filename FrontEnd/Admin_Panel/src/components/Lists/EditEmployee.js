import React from 'react'
import axios from 'axios'
import restConfig from '../config/restConfig'

export default class EditEmployee extends React.Component {
    submitForm = () => {
        let form = document.getElementById('employeeEditForm_' + this.props.editData.empId.data)

        let transferObject = {
            nic: null,
            fName: null,
            mName: null,
            lName: null,
            dob: null,
            address: null,
            district: null,
            divisionSecretariat: null,
            gramaNilaDivision: null,
            designation: null,
            officialName: null,
            dateOfAppointment: null,
            dateOfRetirement: null,
            mobileNo: null,
            homePhone: null,
            whatsappNo: null,
            imoNo: null,
            eduQualification: null,
            profQualification: null
        }
        let i = 0;
        Object.keys(transferObject).forEach(item => {
            console.log(item + " " + i + " : " + form.elements.item(i).value)
            transferObject[item] = form.elements.item(i).value
            i++
        })

        axios.put(restConfig.getRestAPIUrl('employees', this.props.editData.empId.data), transferObject).then((response) => {
            if (response.status === 202) {
                alert('Employee Updated')
                this.props.eraseEmployeeState()
            } else {
                alert('An error has occurred')
            }
        }).catch((error) => {
            console.log(error)
        })

        let obj = {
            role: form.elements['role'].value
        }

        console.log(obj)

        axios.put(restConfig.getRestAPIUrl('accountrole', this.props.editData.empId.data), obj).then((response) => {
            if (response.status !== 202) {
                alert('Role failed to update')
            }
        }).catch((error) => {
            console.log(error)
        })

    }

    render = () => {
        let inputs = []
        let InputFieldsArray = [
            {
                caption: this.props.editData.nic.caption,
                id: "txtNic_" + this.props.editData.nic.data,
                name: "nic",
                type: "text",
                placeholder: "NIC"
            },
            {
                caption: this.props.editData.name.caption,
                subs: [
                    {
                        id: "txtfName_" + this.props.editData.name.data.fName,
                        name: "fName",
                        type: "text",
                        placeholder: "first Name"
                    },
                    {
                        id: "txtmName_" + this.props.editData.name.data.mName,
                        name: "mName",
                        type: "text",
                        placeholder: "Middle Name",
                        optional: true
                    },
                    {
                        id: "txtlName_" + this.props.editData.name.data.lName,
                        name: "lName",
                        type: "text",
                        placeholder: "Last Name"
                    }
                ]
            },
            {
                caption: this.props.editData.dob.caption,
                id: "txtDob_" + this.props.editData.dob.data,
                name: "dob",
                type: "date",
                placeholder: "Date Of Birth",
            },
            {
                caption: this.props.editData.address.caption,
                id: "txtAddress_" + this.props.editData.address.data,
                name: "address",
                type: "text",
                placeholder: "Permanent Address"
            },
            {
                caption: this.props.editData.residential.caption,
                subs: [
                    {
                        id: "txtdistrict_" + this.props.editData.residential.data.district,
                        name: "district",
                        type: "text",
                        placeholder: "District"
                    },
                    {
                        id: "txtdivSecret_" + this.props.editData.residential.data.divisionSecretariat,
                        name: "divisionalSecretariat",
                        type: "text",
                        placeholder: "Divisional Secretariat"
                    },
                    {
                        id: "txtgramaNila_" + this.props.editData.residential.data.gramaNilaDivision,
                        name: "gramaNiladariDivision",
                        type: "text",
                        placeholder: "Grama Niladari Division"
                    }
                ]
            },
            {
                caption: this.props.editData.designation.caption,
                id: "txtDesignation_" + this.props.editData.designation.data,
                name: "designation",
                type: "text",
                placeholder: "Designation"
            },
            {
                caption: this.props.editData.officialName.caption,
                id: "txtOfficialName_" + this.props.editData.officialName.data,
                name: "txtOfficialName",
                type: "text",
                placeholder: "Official Name"
            },
            {
                caption: this.props.editData.dateOfAppointment.caption,
                id: "txtDateOfAppointment_" + this.props.editData.dateOfAppointment.data,
                name: "dateOfAppointment",
                type: "date",
                placeholder: "Date Of Appointment"
            },
            {
                caption: this.props.editData.dateOfRetirement.caption,
                id: "txtDateOfRetirement_" + this.props.editData.dateOfRetirement.data,
                name: "dateOfRetirement",
                type: "date",
                placeholder: "Date Of Retirement",
                optional: true
            },
            {
                caption: this.props.editData.homePhone.caption,
                id: "txtHomePhone_" + this.props.editData.homePhone.data,
                name: "homePhone",
                type: "text",
                placeholder: "Home Phone Number"
            },
            {
                caption: this.props.editData.contactNo.caption,
                subs: [
                    {
                        id: "txtMobileNo_" + this.props.editData.contactNo.data.mobileNo,
                        name: "mobileNo",
                        type: "text",
                        placeholder: "Mobile Number"
                    },
                    {
                        id: "txtWhatsappNo_" + this.props.editData.contactNo.data.whatsappNo,
                        name: "whatsappNo",
                        type: "text",
                        placeholder: "Whatsapp Number",
                        optional: true
                    },
                    {
                        id: "txtImoNo_" + this.props.editData.contactNo.data.imoNo,
                        name: "imoNo",
                        type: "text",
                        placeholder: "Imo Number",
                        optional: true
                    }
                ]
            },
            {
                caption: this.props.editData.eduQualification.caption,
                id: "txtEduQualification_" + this.props.editData.eduQualification.data,
                name: "eduQualification",
                placeholder: "Educational Qualifications",
                type: "textarea",
                rows: 5
            },
            {
                caption: this.props.editData.profQualification.caption,
                id: "txtProfQualification_" + this.props.editData.profQualification.data,
                name: "profQualification",
                placeholder: "Professional Qualifications",
                type: "textarea",
                rows: 5
            },
            {
                caption: this.props.editData.role.caption,
                subs: [
                    {
                        id: "radioBranchAdmin_" + this.props.editData.role.data,
                        name: "role",
                        type: "radio",
                        placeholder: "Branch Admin"
                    },
                    {
                        id: "radioSiteAdmin_" + this.props.editData.role.data,
                        name: "role",
                        type: "radio",
                        placeholder: "Site Admin"
                    },
                    {
                        id: "radioEmployee_" + this.props.editData.role.data,
                        name: "role",
                        type: "radio",
                        placeholder: "Employee"
                    }
                ],
                type: "radio",
                authorized: "Site Admin"
            }
        ]

        let close = null
        let saveChanges = null
        InputFieldsArray.forEach(element => {
            let fields = []
            let subparts = element.subs
            let optional = null
            
            if(element.authorized === undefined || element.authorized === localStorage.getItem('user_type')){
                if (subparts !== undefined) {
                    subparts.forEach(parts => {
                        optional = null
                        if (subparts.type !== "radio") {
                            let idParts = parts.id.split("_")
                            if (parts.optional) {
                                optional = <small className="form-text text-muted">Optional</small>
                            }
    
                            if (parts.type !== "radio") {
                                fields.push(<div key={parts.id} className="col-md-3">
                                    <input type={subparts.type} className="form-control" id={subparts.id} name={idParts[0]} placeholder={subparts.placeholder} defaultValue={idParts[1]} />
                                    {optional}
                                </div>)
                            }
                            else {
                              let t = parts.name.split("_")
                                fields.push(<div key={parts.id} className="form-check form-check-inline">
                                    <input type="radio" id={parts.id} name={parts.name} className="form-check-input" value={parts.placeholder} defaultChecked={parts.placeholder === this.props.editData.role.data} />
                                    <label className="form-check-label">{parts.placeholder}</label>
                                </div>)
                            }
                        }
                    })
                }
                else {
                    if (element.optional) {
                        optional = <small className="form-text text-muted">Optional</small>
                    }
                    let idParts = element.id.split("_")
    
                    if (element.type === "textarea") {
                        fields.push(<div key={element.id} className="col-md-10">
                            <textarea className="form-control" id={element.id} name={idParts[0]} rows={element.rows} defaultValue={idParts[1]} />
                        </div>)
                    }
                    else {
                        fields.push(<div key={element.id} className="col-md-10">
                            <input type={element.type} className="form-control" id={element.id} name={idParts[0]} placeholder={element.placeholder} defaultValue={idParts[1]} />
                            {optional}
                        </div>)
                    }
                }
            }
            else{
                fields.push(<label className="col-form-label">{this.props.editData.role.data}</label>)
            }
            
            let frame = null
            if (element.type !== "radio") {
                frame = <div className="form-group row">
                    <label className="col-md-2 col-form-label">{element.caption}</label>
                    {fields}
                </div>
            }
            else {
                frame = <div className="form-group row">
                    <label className="col-md-2 col-form-label">{element.caption}</label>
                    <div className="col-md-10" style={{ alignSelf: "center" }}>
                        {fields}
                    </div>
                </div>
            }
            inputs.push(frame)
        });


        close = <button type="button" className="btn btn-secondary" data-dismiss="modal" onClick={this.setDefaultValues}>Close</button>
        saveChanges = <button type="submit" className="btn btn-primary" data-dismiss="modal" onClick={this.submitForm} >Save changes</button>

        return (
            <div className="modal fade" id={"editEmployee_" + this.props.editData.empId.data} role="dialog"
                aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                <div className="modal-dialog modal-lg" role="document">
                    <div className="modal-content col-md-12">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLongTitle">{this.props.editData.name.data.fName + " " + this.props.editData.name.data.mName + " " + this.props.editData.name.data.fName + " Employee"}</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body col-md-12">
                            <form id={"employeeEditForm_" + this.props.editData.empId.data}>
                                {inputs}
                            </form>
                            <div className="modal-footer">
                                {close}
                                {saveChanges}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )

    }
}
