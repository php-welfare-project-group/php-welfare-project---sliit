import React from 'react'
import axios from 'axios'
import restConfig from '../config/restConfig'

export default class EditBranch extends React.Component{
    state = {
      departmentList:null
    }

    saveInputs = (e) => {
        this.setState({[e.target.name]:e.target.value})
    }

    submitForm = () => {
        let branchName = document.getElementById('txtBranchName_' + this.props.branchId).value
        let departmentID = document.getElementById('chkDepartmentName_'  + this.props.branchId).value

        let transferObject = {
            name:branchName,
            deptID:departmentID
        }

        console.log(transferObject)
        if((branchName !== this.props.name) | (departmentID !== this.props.deptID)){
            axios.put(restConfig.getRestAPIUrl('branch/' + this.props.branchId),transferObject).then((response)=> {
                if(response.status === 202){
                    alert('Branch Updated')
                    this.props.eraseBranchState()
                }else{
                    alert('An error has occurred')
                }
            })
        }
    }

    setDefaultValues = () => {
        document.getElementById('txtBranchName_' + this.props.branchId).value = this.props.name
        //document.getElementById('chkDepartmentName').value = this.props.deptID
    }

    render = () => {
      let departmentList = null
      if(this.state.departmentList === null){
        axios.get(restConfig.getRestAPIUrl('department')).then((response) => {
            this.setState({departmentList:response.data.data})
        })
      }
      else{
          departmentList =  this.state.departmentList.map((item,i) => {
            if(item.deptID === this.props.deptID){
              return <option key={i} value={item.deptID} selected>{item.deptName}</option>
            }else {
              return <option key={i} value={item.deptID}>{item.deptName}</option>
            }
          })
      }

      return (
        <div className="modal fade" id={"editBranch_" + this.props.branchId} role="dialog"
               aria-labelledby="exampleModalLongTitle" aria-hidden="true">
              <div className="modal-dialog" role="document">
                  <div className="modal-content col-md-12">
                      <div className="modal-header">
                          <h5 className="modal-title" id="exampleModalLongTitle">Edit Branch</h5>
                              <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                              </button>
                      </div>
                      <div className="modal-body col-md-12">
                        <form>
                            <div className="form-group row">
                                <label htmlFor={"txtBranchName_" + this.props.branchId} className="col-md-2 col-form-label">Branch Name</label>
                                <div className="col-md-9 offset-md-1" style={{marginTop:"12px"}}>
                                    <input type="text" className="form-control" id={"txtBranchName_" + this.props.branchId} name="name" onChange={this.saveInputs} defaultValue={this.props.name}/>
                                </div>
                            </div>
                            <div className="form-group row">
                                <label htmlFor={"chkDepartmentName_" + this.props.branchId} className="col-md-2 col-form-label">Department Name</label>
                                <div className="col-md-9 offset-md-1" style={{marginTop:"12px"}}>
                                    <select id={"chkDepartmentName_" + this.props.branchId} name="deptID" className="form-control" onChange={this.saveInputs} style={{overFlow:"scroll"}}>
                                        {departmentList}
                                    </select>
                                </div>
                            </div>
                        </form>
                        <div className="modal-footer">
                          <button type="button" className="btn btn-secondary" data-dismiss="modal" onClick={this.setDefaultValues}>Close</button>
                          <button type="submit" className="btn btn-primary" data-dismiss="modal" onClick={this.submitForm} >Save changes</button>
                        </div>
                    </div>
                 </div>
            </div>
        </div>
      )
    }
}
