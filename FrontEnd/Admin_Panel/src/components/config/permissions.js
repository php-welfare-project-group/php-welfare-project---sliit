const permissionsManager = function(){
    let siteAdminPermissions = {
            'user_type':'Site Admin',
            'permissions':{
                'employees':{
                    'add':true,
                    'delete':true,
                    'update':{
                        'data':true,
                        'status':true
                    },
                    'view':{
                        'all':true,
                        'single':true
                    },
                    'assign':true
                },
                'branches':{
                    'add':true,
                    'delete':true,
                    'update':{
                        'data':true,
                        'status':true
                    },
                    'view':{
                        'all':true,
                        'single':true
                    }
                },
                'sections':{
                    'add':true,
                    'delete':true,
                    'update':{
                        'data':true,
                        'status':true
                    },
                    'view':{
                        'all':true,
                        'single':true
                    }
                }
            }
        }
    
    let branchAdminPermissions = {
            'user_type':'Branch Admin',
            'permissions':{
                'employees':{
                    'add':true,
                    'delete':false,
                    'update':{
                        'data':true,
                        'status':false
                    },
                    'view':{
                        'all':true,
                        'single':false
                    },
                    'assign':false
                },
                'branches':{
                    'add':true,
                    'delete':false,
                    'update':{
                        'data':true,
                        'status':true
                    },
                    'view':{
                        'all':true,
                        'single':true
                    }
                },
                'sections':{
                    'add':true,
                    'delete':false,
                    'update':{
                        'data':true,
                        'status':false
                    },
                    'view':{
                        'all':true,
                        'single':true
                    }
                }
            }
        }

    this.getPermissions = function(user_type){
        if(user_type === "Site Admin"){
            return siteAdminPermissions
        }
        else if(user_type === "Branch Admin"){
            return branchAdminPermissions
        }
        else{
            return "Invalid user type"
        }
    }
}
module.exports = new permissionsManager()