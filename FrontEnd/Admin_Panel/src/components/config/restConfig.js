const config = function () {
    const protocol = "http"
    const host = "localhost"
    const port = "8080"
    const apiUrl = "Welfare_PHP_Admin_Panel/public/api"

    this.getRestAPIBaseUrl = function () {
        /**
         * This is the base api url. If you want to get absolute REST API
         * url, use getRestAPIUrl function
         * @return {string}
         */
        return protocol + "://" + host + ":" + port + "/" + apiUrl
    }
    this.getRestAPIUrl = function (keyword,options) {
        /**
         * @params {
         *      keyword means the REST API controller
         *      options should be passed as an array. If there are some other words
         *      in the url. those things should be passed in this option parameter
         *      eg:http://localhost:80/welfare_chanaka/public/api/branch/count
         *
         *      In above url count is passed in this option params.
         * }
         * @return {string}
         */
        let url = protocol + "://" + host + ":" + port + "/" + apiUrl + "/" + keyword
        if(options !== undefined && options.length > 0){
            for(let i=0;i<options.length;i++){
                url += "/" + options[i]
            }
        }
        return url
    }

}

module.exports = new config()
