let forms = function(){
    this.getEmployeeFormFields = function(item){
        return(
            {
                empId: {
                    caption: "Employee ID",
                    data: item.empId
                },
                nic: {
                    caption: "NIC",
                    data: item.nic
                },
                name: {
                    caption: "Employee Name",
                    data: {
                        fName: item.fName,
                        mName: item.mName,
                        lName: item.lName,
                        delimeter: " "
                    }
                },
                dob: {
                    caption: "Date Of Birth",
                    data: item.dob
                },
                address: {
                    caption: "Address",
                    data: item.address
                },
                residential: {
                    caption: "Residential",
                    data: {
                        district: item.district,
                        divisionSecretariat: item.divisionSecretariat,
                        gramaNilaDivision: item.gramaNilaDivision,
                        delimeter: " "
                    }
                },
                designation: {
                    caption: "Designation",
                    data: item.designation
                },
                officialName: {
                    caption: "Official Name",
                    data: item.officialName
                },
                dateOfAppointment: {
                    caption: "Date Of Appointment",
                    data: item.dateOfAppointment
                },
                dateOfRetirement: {
                    caption: "Date Of Retirement",
                    data: item.dateOfRetirement
                },
                contactNo: {
                    caption: "Contact No",
                    data: {
                        whatsappNo: item.whatsappNo,
                        imoNo: item.imoNo,
                        mobileNo: item.mobileNo,
                        delimeter: ", "
                    }
                },
                homePhone: {
                    caption: "Home Phone",
                    data: item.homePhone
                },
                eduQualification: {
                    caption: "Educational Qualifications",
                    data: item.eduQualification
                },
                profQualification: {
                    caption: "Professional Qualifications",
                    data: item.profQualification
                },
                status: {
                    caption: "Status",
                    data: item.status
                },
                role: {
                    caption: "Role",
                    data: item.role
                }
            }
        )
    }
}

module.exports = new forms()