let sideBarConfig = [
    {
        menuName: "Dashboard",
        menuIcon: "fa-align-justify",
        subMenuID: "subMenuDashboard",
        to:"/dashboard"
    },
    {
        menuName: "Branch",
        menuIcon: "fa-align-justify",
        subMenuID: "subMenuHome1",
        approval: "Site Admin",
        subs: [
            {
                to: "/dashboard/branch/add",
                name: "Add Branch",
                icon: "fa-align-justify"
            },
            {
                to: "/dashboard/branch/list",
                name: "List Branch",
                icon: "fa-align-justify"
            }
        ]
    },
    {
        menuName: "Section",
        menuIcon: "fa-align-justify",
        subMenuID: "subMenuHome2",
        subs: [
            {
                to: "/dashboard/section/add",
                name: "Add Section",
                icon: "fa-align-justify"
            },
            {
                to: "/dashboard/section/list",
                name: "List Section",
                icon: "fa-align-justify"
            }
        ]
    },
    {
        menuName: "Employee",
        menuIcon: "fa-align-justify",
        subMenuID: "subMenuHome3",
        subs: [
            {
              to: "/dashboard/employee/add",
              name: "Add Employee",
              icon: "fa-align-justify"
            },
            {
              to: "/dashboard/employee/list",
              name: "List Employee",
              icon: "fa-align-justify"
            }
        ]
    }]

module.exports = sideBarConfig
