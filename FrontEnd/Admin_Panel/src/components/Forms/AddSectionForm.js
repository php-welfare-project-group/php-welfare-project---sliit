import React from 'react'
import axios from 'axios'
import restConfig from '../config/restConfig'

export default class AddSectionForm extends React.Component {
    state = {
        branchList: null,
        branchId: null,
        sectionName: null,
        sectionStatus: null
    }

    saveInputs = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }

    UNSAFE_componentWillMount = () => {
        let headers = { 'active': true }
        axios.get(restConfig.getRestAPIUrl('branch'), { 'headers': headers }).then((result) => {
            console.log(result)
            this.setState({ branchList: result.data.data })
        })
    }

    submitForm = () => {
        if ((this.state.branchId !== null) && (this.state.sectionName !== null)) {
            let transferObject = {
                sectionName: this.state.sectionName,
                branchId: this.state.branchId,
                status:(localStorage.getItem('user_type') === "Site Admin") ? this.state.sectionStatus : '-2'
            }

            axios.post(restConfig.getRestAPIUrl('section'), transferObject).then((response) => {
                if (response.status === 203) {
                    alert('Section Added')
                } else {
                    alert('An error has occurred')
                }
            })
        }
        console.log(this.state)
    }

    render = () => {
        let branchList = null
        let statusSelector = null

        if (this.state.branchList !== null) {
            branchList = this.state.branchList.map((item, i) => {
                return <option key={i} value={item.branchId}>{item.name}</option>
            })
        }

        if(localStorage.getItem('user_type') === "Site Admin"){
            statusSelector = <div className="form-group row">
                                <label htmlFor="statusSelector" className="col-md-2 col-form-label">Section Status</label>
                                <div className="col-md-10" style={{ alignSelf: "center" }}>
                                    <div className="form-check form-check-inline" id="statusSelector">
                                        <input type="radio" id="radioActive" name="sectionStatus" className="form-check-input" value={1} onChange={this.saveInputs} />
                                        <label className="form-check-label" htmlFor="radioActive">Active</label>
                                    </div>
                                    <div className="form-check form-check-inline" id="statusSelector">
                                        <input type="radio" id="radioDeactive" name="sectionStatus" className="form-check-input" value={0} onChange={this.saveInputs} />
                                        <label className="form-check-label" htmlFor="radioActive">Deactive</label>
                                    </div>
                                </div>
                            </div>
        }
        else{
            statusSelector = null
        }

        return (
            <div className="col-md-12" style={{ padding: "10px" }}>
                <h5>Add Section</h5>
                <hr />
                <form id={this.props.id}>
                    <div className="form-group row">
                        <label htmlFor="txtSectionName" className="col-md-2 col-form-label">Section Name</label>
                        <div className="col-md-10">
                            <input type="text" className="form-control" id="txtSectionName" name="sectionName" placeholder="Section Name" onChange={this.saveInputs} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="txtBranchName" className="col-md-2 col-form-label">Branch Name</label>
                        <div className="col-md-10">
                            <select id="chkBranchName" name="branchId" className="form-control" style={{ overFlow: "scroll" }}
                                onChange={this.saveInputs}>
                                <option disabled={true} selected={true}>Select a Branch</option>
                                {branchList}
                            </select>
                        </div>
                    </div>
                    {statusSelector}
                    <input type="button" className="btn btn-primary" value="Submit" onClick={this.submitForm} />
                </form>
            </div>
        )
    }
}
