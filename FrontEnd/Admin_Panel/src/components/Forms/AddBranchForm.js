import React from 'react'
import axios from 'axios'
import restConfig from '../config/restConfig'

export default class AddBranchForm extends React.Component{
    state = {
        // departmentList:null,
        departmentId:null,
        BranchName: null,
        BranchStatus: null
    }

    saveInputs = (e) => {
        this.setState({[e.target.name]:e.target.value})
    }

    UNSAFE_componentWillMount = () => {
            axios.get(restConfig.getRestAPIUrl('department')).then((result) => {
                console.log(result)
                this.setState({departmentList:result.data.data})
            })
    }

    submitForm = () => {
        if((this.state.BranchName !== null) && (this.state.BranchStatus !== null)){
            let transferObject = {
                name:this.state.BranchName,
                deptID:"1",//this.state.departmentId,
                status:this.state.BranchStatus
            }
            console.log(transferObject)

            axios.post(restConfig.getRestAPIUrl('branch'),transferObject).then((response) => {
                console.log(response)
                if(response.status === 203){
                  alert('Branch Added')
                }else {
                  alert('An error has occurred')
                }
            })
        }
        console.log(this.state)
    }

    render = () => {
    //   let departmentList = null
    //   if(this.state.departmentList !== null){
    //       departmentList = this.state.departmentList.map((item,i) => {
    //           return <option key={i} value={item.deptID}>{item.deptName}</option>
    //       })
    //   }
        return(
            <div className="col-md-12" style={{padding:"10px"}}>
                <h5>Add Branch</h5>
                <hr/>
                <form id={this.props.id}>
                    <div className="form-group row">
                        <label htmlFor="txtBranchName" className="col-md-2 col-form-label">Branch Name</label>
                        <div className="col-md-10">
                            <input type="text" className="form-control" id="txtBranchName" name="BranchName" placeholder="Branch Name" onChange={this.saveInputs}/>
                        </div>
                    </div>
                    {/* <div className="form-group row">
                        <label htmlFor="txtBranchName" className="col-md-2 col-form-label">Department Name</label>
                        <div className="col-md-10">
                            <select id="chkBranchName" name="departmentId" className="form-control" onChange={this.saveInputs}>
                                <option disabled={true} selected={true}>Select a Department</option>
                                {departmentList}
                            </select>
                        </div>
                    </div> */}
                    <div className="form-group row">
                        <label htmlFor="statusSelector" className="col-md-2 col-form-label">Branch Status</label>
                        <div className="col-md-10" style={{alignSelf:"center"}}>
                            <div className="form-check form-check-inline" id="statusSelector">
                                <input type="radio" id="radioActive" name="BranchStatus" className="form-check-input" value={1} onChange={this.saveInputs}/>
                                <label className="form-check-label" htmlFor="radioActive">Active</label>
                            </div>
                            <div className="form-check form-check-inline" id="statusSelector">
                                <input type="radio" id="radioDeactive" name="BranchStatus" className="form-check-input" value={0} onChange={this.saveInputs}/>
                                <label className="form-check-label" htmlFor="radioActive">Deactive</label>
                            </div>
                        </div>
                    </div>
                    <input type="button" className="btn btn-primary" value="Submit" onClick={this.submitForm}/>
                </form>
            </div>
        )
    }
}
