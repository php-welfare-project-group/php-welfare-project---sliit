import React from 'react'
import axios from 'axios'
import restConfig from '../config/restConfig'

export default class AddEmployeeForm extends React.Component {

    state = {
        nic: null,
        fName: null,
        mName: null,
        lName: null,
        dob: null,
        address: null,
        district: null,
        divisionSecretariat: null,
        gramaNilaDivision: null,
        designation: null,
        officialName: null,
        dateOfAppointment: null,
        dateOfRetirement: null,
        mobileNo: null,
        homePhone: null,
        whatsappNo: null,
        imoNo: null,
        eduQualification: null,
        profQualification: null,
        role: null,
        status: null
    }

    saveInputs = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }

    submitForm = () => {
        let transferObject = {
            nic: this.state.nic,
            fName: this.state.fName,
            mName: this.state.mName,
            lName: this.state.lName,
            dob: this.state.dob,
            address: this.state.address,
            district: this.state.district,
            divisionSecretariat: this.state.divisionSecretariat,
            gramaNilaDivision: this.state.gramaNilaDivision,
            designation: this.state.designation,
            officialName: this.state.officialName,
            dateOfAppointment: this.state.dateOfAppointment,
            dateOfRetirement: this.state.dateOfRetirement,
            mobileNo: this.state.mobileNo,
            homePhone: this.state.homePhone,
            whatsappNo: this.state.whatsappNo,
            imoNo: this.state.imoNo,
            eduQualification: this.state.eduQualification,
            profQualification: this.state.profQualification,
            status: (localStorage.getItem('user_type') === "Site Admin") ? this.state.status : '-2'
        }

        axios.post(restConfig.getRestAPIUrl('employees'), transferObject).then((response) => {
            if (response.status === 203) {
                alert('Employee Added')
                this.getEmployeeId(this.state.nic).then((response) => {
                    if (response !== 0) {
                        this.submitAccount(response).then((response) => {
                            if (response !== 0) {
                                alert('Account Created')
                            }
                            else {
                                alert('An error has occurred')
                            }
                        })
                    }
                })
            } else {
                alert('An error has occurred')
            }
        })
    }

    getEmployeeId = (e) => {
        let value = ['nic', e]
        let headers = { 'type': 'Add' }
        return new Promise((resolve, reject) => {
            axios.get(restConfig.getRestAPIUrl('employees', value), { "headers": headers }).then((response) => {
                if (response.data.rowCount > 0) {
                    resolve(response.data.data[0].empId)
                } else {
                    resolve(0)
                }
            }).catch((error) => {
                reject(error)
            })
        })
    }

    submitAccount = (e) => {
        return new Promise((resolve, reject) => {
            let transferData = {
                empId: e,
                userName: this.state.nic,
                password: this.state.nic,
                role: (localStorage.getItem('user_type') === "Site Admin") ? this.state.role : "Employee"
            }
            axios.post(restConfig.getRestAPIUrl('accounts'), transferData).then((result) => {
                if (result.status === 203) {
                    resolve(1)
                }
                else {
                    resolve(0)
                }
            }).catch((error) => {
                reject(error)
            })
        })
    }

    render = () => {
        let roleSelection = ""
        let statusSelection = ""

        if(localStorage.getItem('user_type') === "Site Admin"){
            roleSelection = (<div className="form-group row">
                                <label htmlFor="siteAdmin" className="col-md-2 col-form-label">Role</label>
                                <div className="col-md-10" style={{ alignSelf: "center" }}>
                                    <div className="form-check form-check-inline" id="siteAdmin">
                                        <input type="radio" id="radioSiteAdmin" name="role" className="form-check-input" value="Site Admin" onChange={this.saveInputs} />
                                        <label className="form-check-label" htmlFor="radioSiteAdmin">Site Admin</label>
                                    </div>
                                    <div className="form-check form-check-inline" id="siteAdmin">
                                        <input type="radio" id="radioBranchAdmin" name="role" className="form-check-input" value="Branch Admin" onChange={this.saveInputs} />
                                        <label className="form-check-label" htmlFor="radioBranchAdmin">Branch Admin</label>
                                    </div>
                                    <div className="form-check form-check-inline" id="siteAdmin">
                                        <input type="radio" id="radioEmployee" name="role" className="form-check-input" value="Employee" onChange={this.saveInputs} />
                                        <label className="form-check-label" htmlFor="radioEmployee">Employee</label>
                                    </div>
                                </div>
                            </div>)
            
            statusSelection = (<div className="form-group row">
                                    <label htmlFor="statusSelector" className="col-md-2 col-form-label">Employee Status</label>
                                    <div className="col-md-10" style={{ alignSelf: "center" }}>
                                        <div className="form-check form-check-inline" id="statusSelector">
                                            <input type="radio" id="radioActive" name="status" className="form-check-input" value={1} onChange={this.saveInputs} />
                                            <label className="form-check-label" htmlFor="radioActive">Active</label>
                                        </div>
                                        <div className="form-check form-check-inline" id="statusSelector">
                                            <input type="radio" id="radioDeactive" name="status" className="form-check-input" value={0} onChange={this.saveInputs} />
                                            <label className="form-check-label" htmlFor="radioActive">Deactive</label>
                                        </div>
                                    </div>
                                </div>)                
        }
        else{
            roleSelection = (<div className="form-group row">
                                <label htmlFor="siteAdmin" className="col-md-2 col-form-label">Role</label>
                                <label id="siteAdmin" className="col-md-2 col-form-label">Employee</label>
                            </div>)

            statusSelection = (<div className="form-group row">
                                <label htmlFor="statusSelector" className="col-md-2 col-form-label">Status</label>
                                <label id="statusSelector" className="col-md-2 col-form-label">Deactive</label>
                            </div>)                
        }
        
        return (
            <div className="col-md-12" style={{ padding: "10px" }}>
                <h5>Add Employee</h5>
                <hr />
                <form>
                    <div className="form-group row">
                        <label htmlFor="txtNic" className="col-md-2 col-form-label">NIC</label>
                        <div className="col-md-10">
                            <input type="text" className="form-control" id="txtNic" name="nic" placeholder="NIC Number" onChange={this.saveInputs} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="txtName" className="col-md-2 col-form-label">Name</label>
                        <div className="col-md-3">
                            <input type="text" className="form-control" id="txtfName" name="fName" placeholder="First Name" onChange={this.saveInputs} />
                        </div>
                        <div className="col-md-4">
                            <input type="text" className="form-control" id="txtmName" name="mName" placeholder="Middle Name" onChange={this.saveInputs} />
                            <small className="form-text text-muted">Optional</small>
                        </div>
                        <div className="col-md-3">
                            <input type="text" className="form-control" id="txtlName" name="lName" placeholder="Last Name" onChange={this.saveInputs} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="txtDob" className="col-md-2 col-form-label">Date Of Birth</label>
                        <div className="col-md-10">
                            <input type="date" className="form-control" id="txtDob" name="dob" onChange={this.saveInputs} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="txtAddress" className="col-md-2 col-form-label">Address</label>
                        <div className="col-md-10">
                            <input type="text" className="form-control" id="txtAddress" name="address" placeholder="Permenant Address" onChange={this.saveInputs} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="txtName" className="col-md-2 col-form-label">Residential</label>
                        <div className="col-md-3">
                            <input type="text" className="form-control" id="txtdistrict" name="district" placeholder="District" onChange={this.saveInputs} />
                        </div>
                        <div className="col-md-4">
                            <input type="text" className="form-control" id="txtdivSecret" name="divisionSecretariat" placeholder="Divisional Secretariat" onChange={this.saveInputs} />
                        </div>
                        <div className="col-md-3">
                            <input type="text" className="form-control" id="txtgramaNila" name="gramaNilaDivision" placeholder="Grama Niladhari Division" onChange={this.saveInputs} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="txtDesignation" className="col-md-2 col-form-label">Designation</label>
                        <div className="col-md-10">
                            <input type="text" className="form-control" id="txtDesignation" name="designation" placeholder="Designation" onChange={this.saveInputs} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="txtOfficialName" className="col-md-2 col-form-label">Official Name</label>
                        <div className="col-md-10">
                            <input type="text" className="form-control" id="txtOfficialName" name="officialName" placeholder="Official Name" onChange={this.saveInputs} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="txtDateOfAppointment" className="col-md-2 col-form-label">Date Of Appointment</label>
                        <div className="col-md-10">
                            <input type="date" className="form-control" id="txtDateOfAppointment" name="dateOfAppointment" onChange={this.saveInputs} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="txtDateOfRetirement" className="col-md-2 col-form-label">Date Of Retirement</label>
                        <div className="col-md-10">
                            <input type="date" className="form-control" id="txtDateOfRetirement" name="dateOfRetirement" onChange={this.saveInputs} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="txtMobileNo" className="col-md-2 col-form-label">Contact</label>
                        <div className="col-md-2">
                            <input type="text" className="form-control" id="txtMobileNo" name="mobileNo" placeholder="Mobile No" onChange={this.saveInputs} />
                        </div>
                        <div className="col-md-3">
                            <input type="text" className="form-control" id="txtHomePhone" name="homePhone" placeholder="Home Phone No" onChange={this.saveInputs} />
                        </div>
                        <div className="col-md-3">
                            <input type="text" className="form-control" id="txtWhatsappNo" name="whatsappNo" placeholder="WhatsApp No" onChange={this.saveInputs} />
                            <small className="form-text text-muted">Optional</small>
                        </div>
                        <div className="col-md-2">
                            <input type="text" className="form-control" id="txtImoNo" name="imoNo" placeholder="Imo No" onChange={this.saveInputs} />
                            <small className="form-text text-muted">Optional</small>
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="txtEduQualification" className="col-md-2 col-form-label">Education Qualification</label>
                        <div className="col-md-10">
                            <textarea type="date" className="form-control" id="txtEduQualification" name="eduQualification" rows="5" onChange={this.saveInputs} />
                        </div>
                    </div>
                    <div className="form-group row">
                        <label htmlFor="txtProfQualification" className="col-md-2 col-form-label">Professional Qualification</label>
                        <div className="col-md-10">
                            <textarea type="date" className="form-control" id="txtProfQualification" name="profQualification" rows="5" onChange={this.saveInputs} />
                        </div>
                    </div>
                    {roleSelection}
                    {statusSelection}
                    <input type="button" className="btn btn-primary" value="Submit" onClick={this.submitForm} />
                </form>
            </div>
        )
    }

}
