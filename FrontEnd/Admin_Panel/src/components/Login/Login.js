import React from 'react';
import './Login.css';
import axios from 'axios'
import restConfig from '../config/restConfig.js'

export default class Login extends React.Component {
    state = {
        username: null,
        password: null,
        error:""
    }

    setValues = (e) => {
        e.preventDefault()
        this.setState({[e.target.name]: e.target.value})
    }

    getEmployeeData = (NIC) => {
        return new Promise((resolve,reject) => {
            let options = ['nic', NIC]
            axios.get(restConfig.getRestAPIUrl('employees', options),{'headers' : {'account':true}}).then((result) => {
                resolve(result)
            }).catch((error) => {
                if(error.response === undefined){
                    reject("There is a server error in the system")
                }
                else if(error.response.status === 404){
                    reject("User doesn't exist in the system with this credentials")
                }
            })
        })
    }

    showErrorAlert = (cssClass,error) => {
        let errorAlert = document.getElementById('loginErrorAlert')
        this.setState({error:error})
        errorAlert.className = cssClass
        errorAlert.style.display = "block"
    }

    moveToDashBoard = () => {
        return new Promise((resolve,reject) => {
            this.showErrorAlert("form-group alert alert-success","Login is successfull")
            resolve(true)
        })
    }
    
    hexString = (buffer) => {
    const byteArray = new Uint8Array(buffer);

    const hexCodes = [...byteArray].map(value => {
    const hexCode = value.toString(16);
    const paddedHexCode = hexCode.padStart(2, '0');
    return paddedHexCode;
  });

  return hexCodes.join('');
    }

    authenticate = () => {
        let username = document.getElementById('txtUserName').value
        let password = document.getElementById('txtPassword').value
        let isValidated = ""
        if(username === ''){
            isValidated+= "U"
        }

        if(password === ''){
            isValidated+= "P"
        }

        switch(isValidated){
            case "U":
                this.showErrorAlert("form-group alert alert-danger","Please enter the username")
                break;
            case "P":
                this.showErrorAlert("form-group alert alert-danger","Please enter the password")
                break;
            case "UP":
                this.showErrorAlert("form-group alert alert-danger","Please enter username and password")
                break;
            default:       
             if(username === password){
                this.getEmployeeData(password).then((result) => {
                    let userData = result.data.data[0]
                    const encoder = new TextEncoder();
                    const data = encoder.encode(password);
                    crypto.subtle.digest('SHA-256',data).then((digestValue) => {
                        console.log(this.hexString(digestValue).toString())
                        console.log(userData.password)
                        if(userData.role !== "Employee" && userData.password === this.hexString(digestValue).toString()){
                            localStorage.setItem("user_id",userData.empId)
                            localStorage.setItem("user_type",userData.role)
                            localStorage.setItem("user_name",userData.fName + " " + userData.lName)
                            localStorage.setItem('designation',userData.designation)
                            localStorage.setItem("dob",userData.dob)
                            localStorage.setItem("email",userData.email)
                            this.moveToDashBoard().then((result) => {
                                window.setTimeout(() => {
                                    this.props.loggedIn()
                                },3000)
                            })
                        }
                        else{
                            this.showErrorAlert("form-group alert alert-danger","User is not authorized")
                        }
                    })
                }).catch((error) => {
                    this.setState({error:error})
                })
            }
            else{
                this.showErrorAlert("form-group alert alert-danger","Username or password is invalid")
            };
            break;
        }
    }

    componentWillMount = () => {
        console.log(this.props)
    }
    render = () => {
        return (
            <div className="container" style={{height: "100vh"}}>
                <div className="row h-100 justify-content-center align-items-center">
                    <div className="col-10 col-md-6 loginForm">
                        <h2 className="d-none d-md-block">Welcome to .... System</h2>
                        <div className="form-group">
                            <label><strong>Username</strong></label>
                            <input id="txtUserName" name="username" type="text" className="form-control" placeholder="Username"
                                   onChange={this.setValues}/>
                        </div>
                        <div className="form-group">
                            <label><strong>Password</strong></label>
                            <input id="txtPassword" name="password" type="password" className="form-control" placeholder="Password"
                                   onChange={this.setValues}/>
                        </div>
                        <div id="loginErrorAlert" className="form-group alert alert-danger" style={{display:"none"}}>{this.state.error}</div>
                        <div style={{textAlign:"center"}}>
                            <input type="button" value="Log In" className="btn btn-primary btn-sx btn-col-md-3"
                               onClick={this.authenticate}/>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}