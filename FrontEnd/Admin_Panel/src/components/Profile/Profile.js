import React from 'react'
import ProfilePicture from '../../assets/img/40x40-walnut-main.jpg'

export default class Profile extends React.Component {
    state = {
        firstName:"",
        lastName:"",
        designation:"",
        DOB:"",
        email:""
    }

    saveInputs = (e) => {
        this.setState({[e.target.id]:e.target.value})
    }

    submitForm = () => {
        console.log(this.state)
    }

    componentDidMount = () => {
        const name = localStorage.getItem('user_name').split(" ")
        this.setState({firstName:name[0]})
        this.setState({lastName:name[1]})
        this.setState({designation:localStorage.getItem('designation')})
        this.setState({DOB:localStorage.getItem('dob')})
        // this.setState({email:localStorage.getItem('email')})
    }

    render = () => {
        return (
            <div className="modal fade" id="adminProfile" role="dialog"
                 aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                <div className="modal-dialog" role="document">
                    <div className="modal-content col-md-12">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLongTitle">{localStorage.getItem('user_name')}</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                        </div>
                        <div className="modal-body col-md-12">
                            <form>
                                <div className="form-group" style={{textAlign:"center"}}>
                                    <img src={ProfilePicture} className="rounded-circle" width={100} height={100} alt="Profile"/>
                                </div>
                                <div className="form-group col-md-12">
                                    <label>First Name</label>
                                    <input type="text" className="form-control" value={this.state.firstName} id="firstName" onChange={this.saveInputs}/>
                                </div>
                                <div className="form-group col-md-12">
                                    <label>Last Name</label>
                                    <input type="text" className="form-control" value={this.state.lastName} id="lastName" onChange={this.saveInputs}/>
                                </div>
                                <div className="form-group col-md-12">
                                    <label>Designation</label>
                                    <input type="text" className="form-control" value={this.state.designation} id="designation" onChange={this.saveInputs}/>
                                </div>
                                <div className="form-group col-md-12">
                                    <label>Date of Birth</label>
                                    <input type="date" className="form-control" value={this.state.DOB} id="DOB" onChange={this.saveInputs}/>
                                </div>
                                {/* <div className="form-group col-md-12">
                                    <label>Email</label>
                                    <input type="text" className="form-control" value={this.state.email} id="email" onChange={this.saveInputs}/>
                                </div> */}
                            </form>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="button" className="btn btn-primary" onClick={this.submitForm}>Save changes</button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}