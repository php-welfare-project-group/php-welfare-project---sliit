import React from 'react'
import {BrowserRouter as Router, Route, Redirect} from 'react-router-dom'
import Login from "./Login/Login";
import DashBoard from "./DashBoard/DashBoard"
import DashboardSample from "./Sample/DashboardSample"
import AddBranchForm from "./Forms/AddBranchForm"
import AddSectionForm from "./Forms/AddSectionForm"
import AddEmployeeForm from "./Forms/AddEmployeeForm"
import BranchList from "./Lists/BranchList"
import SectionList from "./Lists/SectionList"
import EmployeeList from "./Lists/EmployeeList"

export default class Main extends React.Component {
    setLoggedIn = () => {
        this.setState({loggedIn: !this.state.loggedIn})
    }

    state = {
        loggedIn: false,
        routes: [
            {
                path: '/dashboard',
                activeSubMenu: 'subMenuDashboard',
                dashboardBody: <DashboardSample/>
            },
            {
                path: '/dashboard/branch/add',
                activeSubMenu: 'subMenuHome1_0',
                dashboardBody: <AddBranchForm/>
            },
            {
                path: '/dashboard/branch/list',
                activeSubMenu: 'subMenuHome1_1',
                dashboardBody: <BranchList/>
            },
            {
                path: '/dashboard/section/add',
                activeSubMenu: 'subMenuHome2_0',
                dashboardBody: <AddSectionForm/>
            },
            {
                path: '/dashboard/section/list',
                activeSubMenu: 'subMenuHome2_1',
                dashboardBody: <SectionList/>
            },
            {
                path: '/dashboard/employee/add',
                activeSubMenu: 'subMenuHome3_0',
                dashboardBody: <AddEmployeeForm/>
            },
            {
                path: '/dashboard/employee/list',
                activeSubMenu: 'subMenuHome3_1',
                dashboardBody: <EmployeeList/>
            }
        ]
    }

    componentWillMount = () => {
        if (localStorage.getItem('logged_in') === null || localStorage.getItem('logged_in') === "false") {
            this.setState({loggedIn: false})
        }
        else {
            this.setState({loggedIn: true})
        }
    }

    componentDidUpdate = (prevProps, prevState, snapshot) => {
        if (prevState.loggedIn !== this.state.loggedIn) {
            this.setState({loggedIn: this.state.loggedIn})

            if (!this.state.loggedIn) {
                // localStorage.setItem('active_menu', null)
                // localStorage.setItem('active_sub_menu', null)
                // localStorage.setItem('active_menu_button', null)
                //localStorage.setItem('live_sub_menu', null)
            }
            localStorage.setItem('logged_in', this.state.loggedIn)
        }
    }

    render = () => {
        const routes = this.state.routes.map((item, i) => {
            return <Route key={i} path={item.path} exact strict render={() => (this.state.loggedIn ? (
                <DashBoard loggedIn={this.setLoggedIn}
                           activeSubMenu={item.activeSubMenu} body={item.dashboardBody}/>) : (<Redirect to="/"/>))}/>
        })
        return (
            <Router>
                <div>
                    <Route path="/" exact strict render={() => (this.state.loggedIn ? (<Redirect to="/dashboard"/>) : (
                        <Login loggedIn={this.setLoggedIn}/>))}/>
                    {routes}
                </div>
            </Router>
        );
    }
}
