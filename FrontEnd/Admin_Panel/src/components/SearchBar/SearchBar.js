import React from 'react';

export default class SearchBar extends React.Component{

    searchData = () => {
        console.log(this.props);
        let value = document.getElementById('txtSearchBar').value
        this.props.searchFunction(value);
        // console.log('Searching data');
    }

    render(){
        return(
            <form >
            <div className="form-group row">
                <div className="col-md-10">
                    <input className="form-control input-lg mb-md-2" id='txtSearchBar' type="search" placeholder="Search" aria-label="Search" />
                </div>
                <button className="btn btn-outline-success mb-md-2" type="button" style={{width:"15%"}} onClick={this.searchData}>Search</button>
            </div>
        </form>
        );
    }
}