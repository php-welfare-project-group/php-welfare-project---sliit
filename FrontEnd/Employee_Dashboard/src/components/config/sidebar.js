let sideBarConfig = [
    {
        menuName: "Wish Wall",
        menuIcon: "fa-align-justify",
        subMenuID: "subMenuHome1",
        subs: [
            {
                to: "/dashboard",
                name: "All",
                icon: "fa-align-justify"
            },
            {
                to: "/dashboard/birthday",
                name: "Birthdays",
                icon: "fa-align-justify"
            },
            {
                to: "/dashboard/promotion",
                name: "Promotions",
                icon: "fa-align-justify"
            },
            {
                to: "/dashboard/retirement",
                name: "Retirement",
                icon: "fa-align-justify"
            }
        ]
    },
    {
        menuName: "Events",
        menuIcon: "fa-align-justify",
        subMenuID: "subMenuHome2",
        subs: [
            {
                to: "/dashboard/upcoming/all",
                name: "All",
                icon: "fa-align-justify"
            },
            {
                to: "/dashboard/upcoming/birthday",
                name: "Birthdays",
                icon: "fa-align-justify"
            },
            {
                to: "/dashboard/upcoming/promotion",
                name: "Promotions",
                icon: "fa-align-justify"
            },
            {
                to: "/dashboard/upcoming/retirement",
                name: "Retirement",
                icon: "fa-align-justify"
            }
        ]
    },
    {
        menuName: "Home",
        menuIcon: "fa-align-justify",
        subMenuID: "subMenuHome3",
        subs: [{
            to: "/",
            name: "Sub Menu 1",
            icon: "fa-align-justify"
        }
        ]
    }]

module.exports = sideBarConfig