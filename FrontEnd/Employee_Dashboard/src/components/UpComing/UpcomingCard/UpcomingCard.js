import React from 'react'
import profile from '../../../assets/img/40x40-walnut-main.jpg'

export default class UpcomingCard extends React.Component{
    render = () => {
        if(this.props.data !== undefined){
            let notifications = this.props.data.map((item,i) => {
                let caption = null

                if(item.type === 'birthday'){
                    caption = item.name + ' is turning ' + item.age + ' years old'
                }
                else if(item.type === 'promotion'){
                    caption = 'You have promoted to a ' + item.designation + ' - ' + item.name
                }
                return(
                    <div className="card-body" key={i}>
                        <img src={profile} width={60} height={60} alt="upcoming_wish_profile" className="rounded-circle" style={{display:"inline-flex"}}/>
                        <p className="card-text"  style={{display:"inline-flex",paddingLeft:"10px"}}>{caption}</p>
                    </div>
                )
            })

            return(
                <div className="card" style={{backgroundColor:"white",marginBottom:"10px"}}>
                    <div className="card-header">
                        <h5 style={{marginBottom:"0px"}}>{this.props.date}</h5>
                    </div>
                    {notifications}
                </div>
            );
        }
        else{
            return(
                <div className="card" style={{backgroundColor:"white",marginTop:"10px",
                    height: "83vh",
                    width: "55vw",
                    paddingTop: "5px"}}>
                    <div className="card-header">
                        <h5 style={{marginBottom:"0px"}}>No Events Found</h5>
                    </div>
                    <div className="row-100 h-100 justify-content-center align-items-center" style={{display:"flex",alignItems:"center",alignContent:"center"}}>
                        <p className="card-text" style={{paddingLeft:"10px"}}>No upcoming {this.props.type}</p>
                    </div>
                </div>
            );
        }
    }
}