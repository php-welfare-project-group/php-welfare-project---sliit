import React from 'react'
import '../DashBoard/DashBoardBody/DashBoardBody.css'
import UpcomingCard from "./UpcomingCard/UpcomingCard";

export default class UpComing extends React.Component {
    state = {
        data: ""
    }
    componentWillMount = () => {
        let dataArray = null
        if(this.props.dataType === 'all'){
            dataArray = [
                {
                    date: "2010-01-01",
                    data: [{
                        name: "Nimal Jayasinghe",
                        age: "28",
                        image: "../../../assets/img/40x40-walnut-main.jpg",
                        type: "birthday"
                    },
                        {
                            name: "Nimal Wimalasooriya",
                            age: "29",
                            image: "../../../assets/img/40x40-walnut-main.jpg",
                            type: "birthday"
                        },
                        {
                            name: "Nimal Jayasinghe",
                            age: "28",
                            image: "../../../assets/img/40x40-walnut-main.jpg",
                            designation:"Adiministrative Officer",
                            type: "promotion"
                        }]
                },
                {
                    date: "2010-01-02",
                    data: [
                        {
                            name: "Nimal Wimalasooriya",
                            age: "29",
                            image: "../../../assets/img/40x40-walnut-main.jpg",
                            type: "birthday"
                        },
                        {
                            name: "Wimal Jayasinghe",
                            image: "../../../assets/img/40x40-walnut-main.jpg",
                            designation:"Assistant Clerck",
                            type: "promotion"
                        }
                    ]
                }
                ,
                {
                    date: "2010-01-03",
                    data: [
                        {
                            name: "Wimal Wimalasooriya",
                            age: "29",
                            image: "../../../assets/img/40x40-walnut-main.jpg",
                            type:"promotion",
                            designation:"Divisional Secretariat"
                        }
                    ]
                }
            ]
        }
        else if (this.props.dataType === 'birthday') {
            dataArray = [
                {
                    date: "2010-01-01",
                    data: [{
                        name: "Nimal Jayasinghe",
                        age: "28",
                        image: "../../../assets/img/40x40-walnut-main.jpg",
                        type: "birthday"
                    },
                        {
                            name: "Nimal Wimalasooriya",
                            age: "29",
                            image: "../../../assets/img/40x40-walnut-main.jpg",
                            type: "birthday"
                        }]
                },
                {
                    date: "2010-01-02",
                    data: [
                        {
                            name: "Nimal Wimalasooriya",
                            age: "29",
                            image: "../../../assets/img/40x40-walnut-main.jpg",
                            type: "birthday"
                        }
                    ]
                }
            ]
        }
        else if(this.props.dataType === 'promotion'){
            dataArray = [
                {
                    date: "2010-01-01",
                    data: [
                        {
                            name: "Nimal Jayasinghe",
                            age: "28",
                            image: "../../../assets/img/40x40-walnut-main.jpg",
                            designation:"Adiministrative Officer",
                            type: "promotion"
                        }
                        ]
                },
                {
                    date: "2010-01-02",
                    data: [
                        {
                            name: "Wimal Jayasinghe",
                            image: "../../../assets/img/40x40-walnut-main.jpg",
                            designation:"Assistant Clerck",
                            type: "promotion"
                        }
                    ]
                }
                ,
                {
                    date: "2010-01-03",
                    data: [
                        {
                            name: "Wimal Wimalasooriya",
                            age: "29",
                            image: "../../../assets/img/40x40-walnut-main.jpg",
                            type:"promotion",
                            designation:"Divisional Secretariat"
                        }
                    ]
                }
            ]
        }
        this.setState({
            data: dataArray
        })
    }
    render = () => {
        if(this.state.data === null){
            return(
                <UpcomingCard type={this.props.dataType}/>
            )
        }

        let bodyContent = this.state.data.map((item, i) => {
            return <UpcomingCard date={item.date} data={item.data} type={this.props.dataType}/>
        })

        return (
            <div style={{padding: "5px", marginBottom: "10px"}}>
                <div className="col-12 scrollbar-hide"
                     style={{
                         overflowY: "scroll",
                         overflowX: "hidden",
                         height: "83vh",
                         width: "55vw",
                         paddingTop: "5px"
                     }}>
                    {bodyContent}
                </div>
            </div>
        );
    }
}