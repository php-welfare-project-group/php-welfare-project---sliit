import React from 'react';
import './Login.css';

export default class Login extends React.Component {
    state = {
        username: null,
        password: null,
    }

    setValues = (e) => {
        e.preventDefault()
        this.setState({[e.target.name]: e.target.value})
    }

    authenticate = () => {
        console.log(this.state)
        if (this.state.username === 'admin' && this.state.password === 'admin') {
            this.props.loggedIn()
        }
    }

    componentWillMount = () => {
        console.log(this.props)
    }
    render = () => {
        return (
            <div className="loginWindow" style={{height: "100vh"}}>
                <div className="row h-100 justify-content-center align-items-center" style={{paddingLeft:"15px",paddingRight:"15px"}}>
                    <div className="col-10 col-md-6 loginForm">
                        <h2 className="d-none d-md-block">Welcome to .... System</h2>
                        <div className="form-group">
                            <label><strong>Username</strong></label>
                            <input name="username" type="text" className="form-control" placeholder="Username"
                                   onChange={this.setValues}/>
                        </div>
                        <div className="form-group">
                            <label><strong>Password</strong></label>
                            <input name="password" type="password" className="form-control" placeholder="Password"
                                   onChange={this.setValues}/>
                        </div>
                        <input type="button" value="Log In" className="btn btn-primary btn-sx"
                               onClick={this.authenticate}/>
                    </div>
                </div>
            </div>
        );
    }
}