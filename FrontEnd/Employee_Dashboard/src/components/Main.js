import React from 'react'
import {BrowserRouter as Router, Route, Redirect} from 'react-router-dom'
import Login from "./Login/Login";
import DashBoard from "./DashBoard/DashBoard"
import DashBoardBody from "./DashBoard/DashBoardBody/DashBoardBody"
import Profile from "./Profile/Profile"
import UpComing from "./UpComing/UpComing";

export default class Main extends React.Component {
    setLoggedIn = () => {
        this.setState({loggedIn: !this.state.loggedIn})
    }

    state = {
        loggedIn: false,
        routes: [
            {
                path: '/dashboard',
                activeSubMenu: 'subMenuHome1_0',
                body:<DashBoardBody dataType="all"/>
            },
            {
                path: '/dashboard/birthday',
                activeSubMenu: 'subMenuHome1_1',
                body:<DashBoardBody dataType="birthday"/>
            },
            {
                path: '/dashboard/promotion',
                activeSubMenu: 'subMenuHome1_2',
                body:<DashBoardBody dataType="promotion"/>
            },
            {
                path: '/dashboard/retirement',
                activeSubMenu: 'subMenuHome1_3',
                body:<DashBoardBody dataType="retirement"/>
            },
            {
                path: '/dashboard/upcoming/all',
                activeSubMenu: 'subMenuHome2_0',
                body:<UpComing dataType="all"/>
            },
            {
                path: '/dashboard/upcoming/birthday',
                activeSubMenu: 'subMenuHome2_1',
                body:<UpComing dataType="birthday"/>
            },
            {
                path: '/dashboard/upcoming/promotion',
                activeSubMenu: 'subMenuHome2_2',
                body:<UpComing dataType="promotion"/>
            },
            {
                path: '/dashboard/upcoming/retirement',
                activeSubMenu: 'subMenuHome2_3',
                body:<UpComing dataType="retirement"/>
            }
        ]
    }

    componentWillMount = () => {
        if (localStorage.getItem('logged_in') === null || localStorage.getItem('logged_in') === "false") {
            this.setState({loggedIn: false})
        }
        else {
            this.setState({loggedIn: true})
        }
    }

    componentDidUpdate = (prevProps, prevState, snapshot) => {
        if (prevState.loggedIn !== this.state.loggedIn) {
            this.setState({loggedIn: this.state.loggedIn})

            if (!this.state.loggedIn) {
                // localStorage.setItem('active_menu', null)
                // localStorage.setItem('active_sub_menu', null)
                // localStorage.setItem('active_menu_button', null)
                localStorage.setItem('live_sub_menu', null)
            }
            localStorage.setItem('logged_in', this.state.loggedIn)
        }
    }

    render = () => {
        const routes = this.state.routes.map((item, i) => {
            return <Route key={i} path={item.path} exact strict render={() => (this.state.loggedIn ? (
                <DashBoard loggedIn={this.setLoggedIn}
                           activeSubMenu={item.activeSubMenu} body={item.body}/>)  : (<Redirect to="/"/>))}/>
        })
        return (
            <Router>
                <div>
                    <Route path="/" exact strict render={() => (this.state.loggedIn ? (<Redirect to="/dashboard"/>) : (
                        <Login loggedIn={this.setLoggedIn}/>))}/>
                    <Route path="/profile" exact strict render={() => (this.state.loggedIn ? (<Profile/>) : (
                        <Login loggedIn={this.setLoggedIn}/>))}/>
                    {routes}
                </div>
            </Router>
        );
    }
}