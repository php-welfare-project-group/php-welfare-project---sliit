import React from 'react'
import Header from "./Header/Header"
import SideBar from "./SideBar/SideBar"
import './DashBoard.css'
import './DashBoardBody/DashBoardBody.css'

export default class DashBoard extends React.Component {
    componentWillMount = () => {
        localStorage.setItem('logged_in', true)
        localStorage.setItem('live_sub_menu', this.props.activeSubMenu.split('_')[0])
        console.log(this.props)
    }

    render = () => {
        return (
            <div className="dashboardWindow" style={{height: "100vh"}}>
                <Header setLoggedIn={this.props.loggedIn}/>
                <div style={{display: "flex"}}>
                    <div style={{display: "inline-flex", padding: "10px 10px 10px 10px"}}
                         className="col-md-2 d-none d-md-block">
                        <SideBar activeSubMenu={this.props.activeSubMenu}/>
                    </div>
                    <div style={{display: "inline-flex", padding: "10px 10px 10px 10px"}} className="col-md-7">
                        {this.props.body}
                    </div>
                    <div className="card d-none d-md-block" style={{marginTop:"20px",marginBottom:"20px",width:"23vw",height:"83vh"}}>
                        <div className="card-header">
                            <h5 style={{marginBottom:"0px"}}>Notifications</h5>
                        </div>
                        <ul className="list-group list-group-flush scrollbar-hide" style={{height:"75vh",overflowY: "scroll", overflowX: "hidden"}}>
                            <li className="list-group-item">Cras justo odio</li>
                            <li className="list-group-item">Dapibus ac facilisis in</li>
                            <li className="list-group-item">Vestibulum at eros</li>
                            <li className="list-group-item">Cras justo odio</li>
                            <li className="list-group-item">Dapibus ac facilisis in</li>
                            <li className="list-group-item">Vestibulum at eros</li>
                            <li className="list-group-item">Cras justo odio</li>
                            <li className="list-group-item">Dapibus ac facilisis in</li>
                            <li className="list-group-item">Vestibulum at eros</li>
                            <li className="list-group-item">Cras justo odio</li>
                            <li className="list-group-item">Dapibus ac facilisis in</li>
                            <li className="list-group-item">Vestibulum at eros</li>
                        </ul>
                    </div>
                </div>
            </div>
        )
    }
}