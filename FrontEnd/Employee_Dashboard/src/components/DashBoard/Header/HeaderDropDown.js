import React from 'react'
import {Link} from 'react-router-dom'
import wisherProfile from '../../../assets/img/40x40-walnut-main.jpg'

export default class HeaderDropDown extends React.Component{
    render = () => {
        return(
            <div className="dropdown show">
                <Link to="/" className="nav-link HeaderLink" href="#" role="button"
                      id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
                      aria-expanded="false">
                    {this.props.caption}
                </Link>
                <div className="dropdown-menu" aria-labelledby="dropdownMenuLink">
                    <Link to={this.props.to} className="dropdown-item" href="#">
                        <div>
                            <img src={wisherProfile} width={50} height={50} style={{display:"inline-flex"}} className="rounded-circle" alt="wish_notification"/>
                            <p style={{display:"inline-flex",paddingLeft:"10px"}}>Person wished to you</p>
                        </div>
                    </Link>
                    <Link to={this.props.to} className="dropdown-item" href="#">
                        <img src={wisherProfile} width={50} height={50} style={{display:"inline-flex"}} className="rounded-circle" alt="wish_notification"/>
                        <p style={{display:"inline-flex",paddingLeft:"10px"}}>Person wished to you</p>
                    </Link>
                    <Link to={this.props.to} className="dropdown-item" href="#">
                        <img src={wisherProfile} width={50} height={50} style={{display:"inline-flex"}} className="rounded-circle" alt="wish_notification"/>
                        <p style={{display:"inline-flex",paddingLeft:"10px"}}>Person wished to you</p>
                    </Link>
                    <div className="dropdown-divider"></div>
                    <div style={{textAlign: "center"}}><Link to={this.props.allLink} style={{color: "#3498db"}}>See
                        More...</Link></div>
                </div>
            </div>
        );
    }
}