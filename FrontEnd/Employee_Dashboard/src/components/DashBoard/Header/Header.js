import React from 'react'
import profile from '../../../assets/img/40x40-walnut-main.jpg'
import {Link} from 'react-router-dom'
import './Header.css'
import HeaderDropDown from "./HeaderDropDown";

export default class Header extends React.Component {
    render = () => {
        return (
            <div>
                <nav className="navbar sticky-top navbar-expand-md navbar-light bg-primary">
                    <Link to="/" className="navbar-brand" href="#"><img className="rounded-circle" src={profile}
                                                                        width={30} height={30}
                                                                        alt="Profile" style={{marginRight: "10px"}}/>Welfare
                        Department</Link>
                    <button className="navbar-toggler" type="button"
                            data-toggle="collapse" data-target="#notificationContent"
                            aria-label="Navbar Toggle Button"
                            aria-controls="navbarContent">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div id="notificationContent" className="collapse navbar-collapse">
                        <ul className="navbar-nav d-none d-md-flex">
                            <li className="nav-item"><Link to="/" className="nav-link HeaderLink">Home</Link></li>
                            <li className="nav-item">
                                <HeaderDropDown caption="My Wishes" to="/" allLink="/dashboard/wishes"/>
                            </li>
                            <li className="nav-item">
                                <HeaderDropDown caption="Notification" to="/" allLink="/"/>
                            </li>
                        </ul>
                        <ul className="navbar-nav ml-auto">
                            <li className="nav-item" style={{marginRight: "10px"}}>
                                <form className="form-inline pl-5 py-2 d-block d-md-none">
                                    <div className="form-group" style={{marginBottom: "0rem", display: "inline-flex"}}>
                                        <input className="form-control" type="text" placeholder="Search" id="txtSearch"
                                               style={{marginRight: "10px"}}/>
                                        <button className="btn btn-success" value="Search">Search</button>
                                    </div>
                                </form>
                                <form className="form-inline d-none d-md-block">
                                    <div className="form-group" style={{marginBottom: "0rem", display: "inline-flex"}}>
                                        <input className="form-control" type="text" placeholder="Search" id="txtSearch"
                                               style={{marginRight: "10px"}}/>
                                        <button className="btn btn-success" value="Search">Search</button>
                                    </div>
                                </form>
                            </li>
                            <li className="nav-item d-none d-md-block">
                                <Link to="/profile" className="nav-link"><img className="rounded-circle" src={profile}
                                                                              width={30} height={30}
                                                                              alt="Profile"/></Link>
                            </li>
                            <li className="nav-item dropdown d-none d-md-block">
                                <Link to="/" className="nav-link dropdown-toggle HeaderLink" role="button"
                                      data-toggle="dropdown" data-target="#dropDownMenu"
                                      aria-haspopup="true" aria-expanded="false"
                                      id="dropDownButton">User Account</Link>
                                <div className="dropdown-menu dropdown-menu-right" aria-labelledby="dropDownButton">
                                    <Link to="/profile" className="dropdown-item">Account</Link>
                                    <button className="dropdown-item" onClick={this.props.setLoggedIn}>Log Out</button>
                                    <div className="dropdown-divider"></div>
                                    <button className="dropdown-item">Other</button>
                                </div>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        )
    }
}