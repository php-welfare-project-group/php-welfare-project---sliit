import React from 'react'
import './SideBar.css'
import SideBarSubMenu from "./SideBarSubMenu";
import sideBarConfig from '../../config/sidebar'

export default class SideBar extends React.Component {
    states = {
        hasUpdates:false
    }
    componentDidMount = () => {
        console.log(this.props)
        console.log(this.props.activeSubMenu.split('_')[0])
        document.getElementById('list__' + this.props.activeSubMenu.split('_')[0]).setAttribute('aria-expanded', "true")
        document.getElementById('button__' + this.props.activeSubMenu.split('_')[0]).className = "fas fa-angle-left menuArrowIcon-down"
    }

    toggleSubMenu = ({target}) => {
        console.log(target)
        console.log('name : ' + target.id)
        let subMenuID = target.id.split('__')[1]
        let button = document.getElementById('button__' + subMenuID)

        if (button !== null) {
            console.log(button.className)
            if (button.className === 'fas fa-angle-left menuArrowIcon-left') {
                button.className = 'fas fa-angle-left menuArrowIcon-down'
            }
            else {
                button.className = 'fas fa-angle-left menuArrowIcon-left'
            }
        }
    }

    render() {
        let newBadge = null

        if(this.states.hasUpdates){
            newBadge = <span className="badge badge-primary">new</span>
        }

        let sideBarMenu = sideBarConfig.map((item, i) => {
            return (
                <ul key={"SideMenu_" + i} className="list-unstyled components" style={{backgroundColor:"white",marginTop:"10px",marginBottom:"10px"}}>
                <li id={'list__' + item.subMenuID} key={i} href={'#' + item.subMenuID} data-toggle="collapse"
                    aria-expanded="false" onClick={this.toggleSubMenu} style={{cursor:"pointer"}}>
                    <div id={'DivMenu__' + item.subMenuID} className="sideBarMenuItem">
                        <a id={'Menu__' + item.subMenuID} href={'#' + item.subMenuID} data-toggle="collapse"
                           aria-expanded="false"
                           className="sideBarMenuItem" style={{color:"black"}}>
                            <strong id={'Name__' + item.subMenuID}><i id={'Icon__' + item.subMenuID} className={'fas ' + item.menuIcon}></i> {item.menuName} {newBadge}</strong>
                            <i id={'button__' + item.subMenuID} className="fas fa-angle-left menuArrowIcon-left"></i>
                        </a>
                    </div>
                    <hr className="sideBarHr"/>
                    <SideBarSubMenu id={item.subMenuID} subs={item.subs} activeSubMenu={this.props.activeSubMenu} hasUpdates={this.states.hasUpdates}/>
                </li>
                </ul>
            )
        })

        return (
            <div className="col-12" style={{padding: "0px 0px 0px 0px"}}>
                {sideBarMenu}
            </div>
        )
    }
}