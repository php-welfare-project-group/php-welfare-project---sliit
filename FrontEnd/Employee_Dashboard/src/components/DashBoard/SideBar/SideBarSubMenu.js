import React from 'react'
import {Link} from 'react-router-dom'
import './SideBar.css'

export default class SideBarSubMenu extends React.Component{
    render = () => {
        console.log(this.props.id)
        let subMenuItems = this.props.subs.map((item,i) => {
            let color = "";

            let countBadge = null

            if(this.props.hasUpdates){
                countBadge = <span className="badge badge-primary" style={{borderRadius:"0.5rem"}}>1</span>
            }

            console.log(this.props.activeSubMenu + '===' + this.props.id + '_' + i)
            console.log(this.props.activeSubMenu === this.props.id + '_' + i)
            if(this.props.activeSubMenu === this.props.id + '_' + i){
                ///*#007bff;*/ #3498db; /*#28a745;*/
                color = '#3498db'
            }
            else{
                color = ""
            }

            return(
                <li key={i} className="sideBarMenuItem">
                    <Link id={this.props.id + '_' + i} to={item.to} className="sideBarSubMenuItem" style={{color:color}}><i className={'fas ' + item.icon}></i> {item.name} {countBadge}</Link>
                </li>
            )
        })
        let listClass = ""

        console.log((this.props.id + '=== "list-unstyled collapse"'))
        console.log((this.props.id === "list-unstyled collapse"))
        if(this.props.id === localStorage.getItem('live_sub_menu')){
            listClass = "list-unstyled collapse show"
        }
        else{
            listClass = "list-unstyled collapse"
        }
        console.log(document.getElementById(this.props.id));
        return(
            <ul id={this.props.id} className={listClass}>
                {subMenuItems}
            </ul>
        );
    }
}