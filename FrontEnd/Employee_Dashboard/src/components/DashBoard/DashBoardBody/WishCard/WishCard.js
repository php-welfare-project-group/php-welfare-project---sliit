import React from 'react'
import wishCardImage from '../../../../assets/img/merry-christmas-greeting-card-holly-flower-fir-garland-bow-ribbon-gift-boxes-striped-sock-santa-claus-statue-gingerbread-126484608.jpg'

export default class WishCard extends React.Component{
    state = {
        reactedDate:"",
        reactedTime:""
    }

    doWishReacton = () => {
        let icon = document.getElementById(this.props.id)

        if(icon.className === 'far fa-heart'){
            icon.className = 'fas fa-heart'
            this.setState({reactedDate:"2010-01-01"})
            this.setState({reactedTime:"10:00:PM"})
        }
        else{
            icon.className = 'far fa-heart'
            this.setState({reactedDate:""})
            this.setState({reactedTime:""})
        }
    }

    render = () => {
        let wishText = ''

        if((this.state.reactedDate !== "") & (this.state.reactedTime !== "")){
            wishText = '(wished on ' + this.state.reactedDate + ' ' + this.state.reactedTime + ')'
        }
        else{
            wishText = ''
        }

        return(
            <div style={{backgroundColor:"white",padding:"5px",marginBottom:"10px"}}>
                <div className="card">
                    <img className="card-img-top d-md-none" src={wishCardImage} alt="Wish Card" height={250} width={150}/>
                    <img className="card-img-top d-none d-sm-none d-md-block" src={wishCardImage} alt="Wish Card" height={350} width={150}/>
                    <div className="card-body">
                        <p className="card-text"><strong>Wished On 2010-01-10</strong> (2 days ago)</p>
                        <p id={this.props.id + '_para'} onClick={this.doWishReacton} style={{color:"blue",cursor:"pointer",marginBottom:"0px"}}><i id={this.props.id} className="far fa-heart"></i> Wish <strong>{wishText}</strong></p>
                    </div>
                </div>
            </div>
        )
    }
}