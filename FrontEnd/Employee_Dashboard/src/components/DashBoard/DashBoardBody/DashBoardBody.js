import React from 'react'
import './DashBoardBody.css'
import WishCard from "./WishCard/WishCard";

export default class DashBoardBody extends React.Component {
    state = {
        data: null
    }

    componentWillMount = () => {
        console.log(this.props)
        if (this.props.dataType === "all") {
            this.setState({data: "all"})
        }
        else if (this.props.dataType === "birthday") {
            this.setState({data: "birthday"})
        }
        else if (this.props.dataType === "promotion") {
            this.setState({data: "promotion"})
        }
        else if (this.props.dataType === "retirement") {
            this.setState({data: "retirement"})
        }
    }

    render() {
        console.log(this.state.data)
        return (
            <div id="SampleBody" className="col-12"
                 style={{padding: "10px 10px 10px 10px"}}>
                <div className="col-12 scrollbar-hide"
                     style={{overflowY: "scroll", overflowX: "hidden", height: "83vh"}}>
                    <WishCard id="wish_card_1"/>
                    <WishCard id="wish_card_2"/>
                    <WishCard id="wish_card_3"/>
                </div>
            </div>
        )
    }
}