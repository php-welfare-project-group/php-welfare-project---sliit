import React from 'react'
import '../DashBoard/DashBoard.css'
import Header from "../DashBoard/Header/Header"
import './Profile.css'
import '../DashBoard/DashBoardBody/DashBoardBody.css'
import profilePicture from '../../assets/img/40x40-walnut-main.jpg'

export default class Profile extends React.Component {
    state = {
        firstName: "",
        lastName: "",
        email: "",
        designation: "",
        phoneNo: "",
        DOB: "",
        qualifications: ""
    }

    saveValue = (e) => {
        e.preventDefault()
        let property = e.target.id
        this.setState({[property]: e.target.value})
    }

    submitForm = () => {
        console.log(this.state)
    }

    render = () => {
        return (
            <div className="dashboardWindow" style={{height: "100vh"}}>
                <Header/>
                <div className="profileWindow d-none d-md-block">
                    <h4 style={{padding: "10px"}}>Profile</h4>
                    <hr className="profileHeadingHr"/>
                    <div className="col-md-12" style={{marginTop: "10px"}}>
                        <div className="profileBodyLeftBar col-md-3">
                            <img src={profilePicture} className="img-thumbnail rounded-circle d-none d-md-block" alt="profile_desktop"/>
                        </div>
                        <div className="profileBodyRightBar col-md-9">
                            <div className="col-12">
                                <nav>
                                    <div className="nav nav-tabs" id="nav-tab" role="tablist">
                                        <a className="nav-item nav-link active" id="nav-home-tab" data-toggle="tab"
                                           href="#nav-general" role="tab" aria-controls="nav-home"
                                           aria-selected="true">General</a>
                                        <a className="nav-item nav-link" id="nav-profile-tab" data-toggle="tab"
                                           href="#nav-wishes" role="tab" aria-controls="nav-profile"
                                           aria-selected="false">Wishes</a>
                                    </div>
                                </nav>
                                <div className="tab-content" id="nav-tabContent" style={{padding: "5px"}}>
                                    <div className="tab-pane fade show active" id="nav-general" role="tabpanel"
                                         aria-labelledby="nav-general-tab">
                                        <form>
                                            <div className="form-group">
                                                <div className="col-md-6"
                                                     style={{display: "inline-flex", alignItems: "center"}}>
                                                    <label className="col-md-3">First Name</label>
                                                    <input type="text" id="firstName" className="form-control col-md-9"
                                                           placeholder="First Name" onChange={this.saveValue}/>
                                                </div>
                                                <div className="col-md-6"
                                                     style={{display: "inline-flex", alignItems: "center"}}>
                                                    <label className="col-md-3">Last Name</label>
                                                    <input type="text" id="lastName" className="form-control col-md-9"
                                                           placeholder="Last Name" onChange={this.saveValue}/>
                                                </div>
                                            </div>
                                            <div className="form-group">
                                                <div className="col-md-6"
                                                     style={{display: "inline-flex", alignItems: "center"}}>
                                                    <label className="col-md-3">Email</label>
                                                    <input type="text" id="email" className="form-control col-md-9"
                                                           placeholder="Email" onChange={this.saveValue}/>
                                                </div>
                                                <div className="col-md-6"
                                                     style={{display: "inline-flex", alignItems: "center"}}>
                                                    <label className="col-md-3">Designation</label>
                                                    <input type="text" id="designation"
                                                           className="form-control col-md-9" placeholder="Designation"
                                                           onChange={this.saveValue}/>
                                                </div>
                                            </div>
                                            <div className="form-group">
                                                <div className="col-md-6"
                                                     style={{display: "inline-flex", alignItems: "center"}}>
                                                    <label className="col-md-3">Phone Number</label>
                                                    <input type="text" id="phoneNo" className="form-control col-md-9"
                                                           placeholder="Phone Number" onChange={this.saveValue}/>
                                                </div>
                                                <div className="col-md-6"
                                                     style={{display: "inline-flex", alignItems: "center"}}>
                                                    <label className="col-md-3">Date Of Birth</label>
                                                    <input type="date" id="DOB" className="form-control col-md-9"
                                                           placeholder="Last Name" onChange={this.saveValue}/>
                                                </div>
                                            </div>
                                            <div className="form-group">
                                                <div className="col-md-12"
                                                     style={{display: "inline-flex", alignItems: "center"}}>
                                                    <label className="col-md-2">Qualifications</label>
                                                    <textarea className="col-md-10" id="qualifications"
                                                              onChange={this.saveValue}></textarea>
                                                </div>
                                            </div>
                                            <div className="col-md-12" style={{textAlign: "center"}}>
                                                <input type="button" className="btn btn-primary col-md-3" value="Submit"
                                                       onClick={this.submitForm}/>
                                            </div>
                                        </form>
                                    </div>
                                    <div className="tab-pane fade scrollbar-hide" id="nav-wishes" role="tabpanel"
                                         aria-labelledby="nav-wishes-tab"
                                         style={{maxHeight: "400px", overflowY: "scroll"}}>
                                        <div className="col-12" style={{
                                            border: "#3498db 1px solid",
                                            padding: "5px",
                                            marginTop: "2.5px",
                                            marginBottom: "2.5px"
                                        }}>
                                            <label className="col-12" style={{padding: "0px"}}><strong>Minister has sent
                                                a wish for your promotion</strong></label>
                                            <label className="col-12" style={{padding: "0px"}}>2010-01-01</label>
                                            <label className="col-12" style={{padding: "0px"}}>8.00 AM</label>
                                        </div>
                                        <div className="col-12" style={{
                                            border: "#3498db 1px solid",
                                            padding: "5px",
                                            marginTop: "2.5px",
                                            marginBottom: "2.5px"
                                        }}>
                                            <label className="col-12" style={{padding: "0px"}}><strong>Minister has sent
                                                a wish for your promotion</strong></label>
                                            <label className="col-12" style={{padding: "0px"}}>2010-01-01</label>
                                            <label className="col-12" style={{padding: "0px"}}>8.00 AM</label>
                                        </div>
                                        <div className="col-12" style={{
                                            border: "#3498db 1px solid",
                                            padding: "5px",
                                            marginTop: "2.5px",
                                            marginBottom: "2.5px"
                                        }}>
                                            <label className="col-12" style={{padding: "0px"}}><strong>Minister has sent
                                                a wish for your promotion</strong></label>
                                            <label className="col-12" style={{padding: "0px"}}>2010-01-01</label>
                                            <label className="col-12" style={{padding: "0px"}}>8.00 AM</label>
                                        </div>
                                        <div className="col-12" style={{
                                            border: "#3498db 1px solid",
                                            padding: "5px",
                                            marginTop: "2.5px",
                                            marginBottom: "2.5px"
                                        }}>
                                            <label className="col-12" style={{padding: "0px"}}><strong>Minister has sent
                                                a wish for your promotion</strong></label>
                                            <label className="col-12" style={{padding: "0px"}}>2010-01-01</label>
                                            <label className="col-12" style={{padding: "0px"}}>8.00 AM</label>
                                        </div>
                                        <div className="col-12" style={{
                                            border: "#3498db 1px solid",
                                            padding: "5px",
                                            marginTop: "2.5px",
                                            marginBottom: "2.5px"
                                        }}>
                                            <label className="col-12" style={{padding: "0px"}}><strong>Minister has sent
                                                a wish for your promotion</strong></label>
                                            <label className="col-12" style={{padding: "0px"}}>2010-01-01</label>
                                            <label className="col-12" style={{padding: "0px"}}>8.00 AM</label>
                                        </div>
                                        <div className="col-12" style={{
                                            border: "#3498db 1px solid",
                                            padding: "5px",
                                            marginTop: "2.5px",
                                            marginBottom: "2.5px"
                                        }}>
                                            <label className="col-12" style={{padding: "0px"}}><strong>Minister has sent
                                                a wish for your promotion</strong></label>
                                            <label className="col-12" style={{padding: "0px"}}>2010-01-01</label>
                                            <label className="col-12" style={{padding: "0px"}}>8.00 AM</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="profileWindow d-md-none">
                    <div className="col-12" style={{textAlign: "center"}}>
                        <img src={profilePicture} className="rounded-circle d-md-none" width={100} height={100} alt="profile_mobile"/>
                    </div>
                    <div className="col-12" style={{textAlign: "center"}}>
                        <label><strong>First Name Last Name</strong></label>
                    </div>
                    <hr style={{margin: "15px"}}/>
                    <div className="col-12" style={{paddingLeft: "0px", paddingRight: "0px"}}>
                        <nav>
                            <div className="nav nav-tabs" id="nav-tab" role="tablist">
                                <a className="nav-item nav-link active" id="nav-home-tab" data-toggle="tab"
                                   href="#nav-general-mobile" role="tab" aria-controls="nav-home"
                                   aria-selected="true">General</a>
                                <a className="nav-item nav-link" id="nav-profile-tab" data-toggle="tab"
                                   href="#nav-wishes-mobile" role="tab" aria-controls="nav-profile"
                                   aria-selected="false">Wishes</a>
                            </div>
                        </nav>
                        <div className="tab-content" id="nav-tabContent" style={{padding: "5px"}}>
                            <div className="tab-pane fade show active" id="nav-general-mobile" role="tabpanel"
                                 aria-labelledby="nav-general-tab">
                                <div className="col-12"
                                     style={{display: "flex", paddingTop: "5px", paddingBottom: "5px"}}>
                                    <div className="col-4">
                                        <label>Email</label>
                                    </div>
                                    <div className="col-2">
                                        <label> : </label>
                                    </div>
                                    <div className="col-4" style={{paddingLeft: "0px"}}>
                                        <label>xxxxx@gmail.com</label>
                                    </div>
                                </div>
                                <div className="col-12"
                                     style={{display: "flex", paddingTop: "5px", paddingBottom: "5px"}}>
                                    <div className="col-4">
                                        <label>Designation</label>
                                    </div>
                                    <div className="col-2">
                                        <label> : </label>
                                    </div>
                                    <div className="col-4" style={{paddingLeft: "0px"}}>
                                        <label>xxxxxxxx</label>
                                    </div>
                                </div>
                                <div className="col-12"
                                     style={{display: "flex", paddingTop: "5px", paddingBottom: "5px"}}>
                                    <div className="col-4">
                                        <label>Telephone Number</label>
                                    </div>
                                    <div className="col-2">
                                        <label> : </label>
                                    </div>
                                    <div className="col-4" style={{paddingLeft: "0px"}}>
                                        <label>+94xxxxxxxxxx</label>
                                    </div>
                                </div>
                                <div className="col-12"
                                     style={{display: "flex", paddingTop: "5px", paddingBottom: "5px"}}>
                                    <div className="col-4">
                                        <label>Birthday</label>
                                    </div>
                                    <div className="col-2">
                                        <label> : </label>
                                    </div>
                                    <div className="col-4" style={{paddingLeft: "0px"}}>
                                        <label>xxxx-xx-xx</label>
                                    </div>
                                </div>
                                <div className="col-12"
                                     style={{display: "flex", paddingTop: "5px", paddingBottom: "5px"}}>
                                    <div className="col-4">
                                        <label>Qualifications</label>
                                    </div>
                                    <div className="col-2">
                                        <label> : </label>
                                    </div>
                                    <div className="col-4" style={{paddingLeft: "0px"}}>
                                        <ul style={{paddingLeft: "20px"}}>
                                            <li>Item 1</li>
                                            <li>Item 1</li>
                                            <li>Item 1</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="tab-pane fade scrollbar-hide" id="nav-wishes-mobile" role="tabpanel"
                                 aria-labelledby="nav-wishes-tab" style={{maxHeight: "400px", overflow: "scroll"}}>
                                <div className="col-12" style={{
                                    border: "#3498db 1px solid",
                                    padding: "5px",
                                    marginTop: "2.5px",
                                    marginBottom: "2.5px"
                                }}>
                                    <label className="col-12" style={{padding: "0px"}}><strong>Minister has sent a wish
                                        for your promotion</strong></label>
                                    <label className="col-12" style={{padding: "0px"}}>2010-01-01</label>
                                    <label className="col-12" style={{padding: "0px"}}>8.00 AM</label>
                                </div>
                                <div className="col-12" style={{
                                    border: "#3498db 1px solid",
                                    padding: "5px",
                                    marginTop: "2.5px",
                                    marginBottom: "2.5px"
                                }}>
                                    <label className="col-12" style={{padding: "0px"}}><strong>Minister has sent a wish
                                        for your promotion</strong></label>
                                    <label className="col-12" style={{padding: "0px"}}>2010-01-01</label>
                                    <label className="col-12" style={{padding: "0px"}}>8.00 AM</label>
                                </div>
                                <div className="col-12" style={{
                                    border: "#3498db 1px solid",
                                    padding: "5px",
                                    marginTop: "2.5px",
                                    marginBottom: "2.5px"
                                }}>
                                    <label className="col-12" style={{padding: "0px"}}><strong>Minister has sent a wish
                                        for your promotion</strong></label>
                                    <label className="col-12" style={{padding: "0px"}}>2010-01-01</label>
                                    <label className="col-12" style={{padding: "0px"}}>8.00 AM</label>
                                </div>
                                <div className="col-12" style={{
                                    border: "#3498db 1px solid",
                                    padding: "5px",
                                    marginTop: "2.5px",
                                    marginBottom: "2.5px"
                                }}>
                                    <label className="col-12" style={{padding: "0px"}}><strong>Minister has sent a wish
                                        for your promotion</strong></label>
                                    <label className="col-12" style={{padding: "0px"}}>2010-01-01</label>
                                    <label className="col-12" style={{padding: "0px"}}>8.00 AM</label>
                                </div>
                                <div className="col-12" style={{
                                    border: "#3498db 1px solid",
                                    padding: "5px",
                                    marginTop: "2.5px",
                                    marginBottom: "2.5px"
                                }}>
                                    <label className="col-12" style={{padding: "0px"}}><strong>Minister has sent a wish
                                        for your promotion</strong></label>
                                    <label className="col-12" style={{padding: "0px"}}>2010-01-01</label>
                                    <label className="col-12" style={{padding: "0px"}}>8.00 AM</label>
                                </div>
                                <div className="col-12" style={{
                                    border: "#3498db 1px solid",
                                    padding: "5px",
                                    marginTop: "2.5px",
                                    marginBottom: "2.5px"
                                }}>
                                    <label className="col-12" style={{padding: "0px"}}><strong>Minister has sent a wish
                                        for your promotion</strong></label>
                                    <label className="col-12" style={{padding: "0px"}}>2010-01-01</label>
                                    <label className="col-12" style={{padding: "0px"}}>8.00 AM</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}