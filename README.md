# PHP Welfare Project - SLIIT

Supervisor - Mr Jagath Wickramarathne : jagath.w@sliit.lk

BackEnd Technologies (API)
---------------------
* PHP - SLIM Framework(Latest Version)
* SQL DB

FrontEnd Technologies
--------------------
* React JS
* HTML, CSS, JS 

Team Members -

IT16110380 - P.H.R Dharmasena : ravinduta@gmail.com(Leader)

IT16133914 - L.L.K.S Lokuge : kaweelenaduwa@gmail.com

IT16105744 - Witharana T.L.C : lak.deve94@gmail.com

IT18141016 - Wickramasinghe H.C.P. : blcchanaka@gmail.com
